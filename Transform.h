#ifndef TRANSFORM_H_
#define TRANSFORM_H_

#include <DirectXMath.h>

class Transform
{
private:
	DirectX::XMVECTOR pos; // Position vector
	DirectX::XMVECTOR rot; // Rotation quaternion
	DirectX::XMVECTOR sca; // Scale vector
public:
	Transform();
	Transform(const Transform& copy);
	Transform(DirectX::XMVECTOR position,
	          DirectX::XMVECTOR rotation = DirectX::XMQuaternionIdentity(),
	          DirectX::XMVECTOR scale = DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f));
	~Transform();

	Transform& operator= (const Transform& copy);
	friend Transform operator* (const Transform& a, const Transform& b);

	inline DirectX::XMVECTOR __vectorcall GetPosition() const { return pos; }
	inline DirectX::XMVECTOR __vectorcall GetRotation() const { return rot; }
	inline DirectX::XMVECTOR __vectorcall GetScale()    const { return sca; }

	inline void __vectorcall SetPosition(DirectX::XMVECTOR position) { pos = position; }
	inline void __vectorcall SetRotation(DirectX::XMVECTOR rotation) { rot = rotation; }
	inline void __vectorcall SetScale(DirectX::XMVECTOR scale)       { sca = scale; }

	DirectX::XMVECTOR __vectorcall ForwardVector() const;
	DirectX::XMVECTOR __vectorcall RightVector() const;
	DirectX::XMVECTOR __vectorcall UpVector() const;

	// Translate with respect to self
	void __vectorcall TranslateLocal(DirectX::XMVECTOR v);
	// Translate with respect to world
	void __vectorcall TranslateGlobal(DirectX::XMVECTOR v);
	// Rotate with respect to self
	void __vectorcall RotateLocal(DirectX::XMVECTOR q);
	// Rotate with respect to world
	void __vectorcall RotateGlobal(DirectX::XMVECTOR q);
	// Add vector to scale
	void __vectorcall ScaleAdd(DirectX::XMVECTOR v);
	// Multiply scale by vector factor
	void __vectorcall ScaleMul(DirectX::XMVECTOR v);

	DirectX::XMFLOAT4X4 GetMatrix() const;
};

#endif