#pragma once

#include <DirectXMath.h>

// --------------------------------------------------------
// Represents scenewide light like a sun
//
// --------------------------------------------------------
struct DirectionalLight
{
	DirectX::XMFLOAT4 AmbientColor;			// Color of the light
	DirectX::XMFLOAT4 DiffuseColor;			// Color of the environment
	DirectX::XMFLOAT3 Direction;			// The uv of the vertex
};

struct PointLight
{
	DirectX::XMFLOAT4 DiffuseColor;			// Color of the environment
	DirectX::XMFLOAT3 Position;				// The worldspace xyz position of the light
};

struct SpotLight
{
	DirectX::XMFLOAT4 DiffuseColor;			// Color of the environment
	DirectX::XMFLOAT3 Direction;
	float Power;							// how wide the angle is (1=appx 45deg?)
	DirectX::XMFLOAT3 Position;				// The worldspace xyz position of the light
};