#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <DirectXMath.h>

struct ParticleVertex
{
	DirectX::XMFLOAT3 Position;
	DirectX::XMFLOAT2 UV;
	DirectX::XMFLOAT4 Color;
	float Size;
};

struct Particle
{
	// Movement and position
	DirectX::XMVECTOR acc;
	DirectX::XMVECTOR vel;
	DirectX::XMVECTOR pos;

	// Color
	DirectX::XMVECTOR colBeg;
	DirectX::XMVECTOR colEnd;

	// Scaling
	float sizBeg;
	float sizEnd;
	float sizCur;

	// Lifetime
	float time; // Current time spent alive
	float life; // Max time for this particle
};

#endif