#include "SimpleTexture.h"
#include "DDSTextureLoader.h"



SimpleTexture::SimpleTexture() :
	r(nullptr)
{
	// Nothing interesting to do here
}

SimpleTexture::SimpleTexture(ID3D11Device* device, ID3D11DeviceContext* context, const wchar_t* fp) :
	r(nullptr)
{
	if (wcsstr(fp, L".dds") != 0)//itsa skybox!
		DirectX::CreateDDSTextureFromFile(device, fp, nullptr, &r, 0);
	else
		DirectX::CreateWICTextureFromFile(device, context, fp, nullptr, &r, 0);
}

SimpleTexture::~SimpleTexture()
{
	r->Release();
}