#ifndef PHYSICSENTITY_H_
#define PHYSICSENTITY_H_

#include "Entity.h"

#define BT_NO_SIMD_OPERATOR_OVERLOADS
#include "btBulletDynamicsCommon.h"

#include <utility>

class Physics;
class PhysicsEntity;
class Script;
class PhysicsScript;

typedef void(*CollisionCallback)(Script* listener, PhysicsEntity* a, PhysicsEntity* b);

class PhysicsEntity : public Entity
{
private:
	// Managed by Physics
	btRigidBody* body;

	// Managed by the entity
	btCollisionShape* shape;

	// Just a nice pointer to Physics
	Physics* physics;

	// TODO : Setup an event listener system
	std::vector<PhysicsScript*> enterListeners;
	std::vector<PhysicsScript*> exitListeners;

	// Keep track of some persistent physical properties
	float mass;
	float restitution;

	// Updates internals to prepare for physics calculations
	void PreStep();

	// Makes necessary adjustments after physics calculations
	void PostStep();

	// Updates the RigidBody when changes happen
	void UpdateRigidBody();

	friend class Physics;
public:
	PhysicsEntity();
	PhysicsEntity(Game* g, Physics* p, const Transform& t = Transform(), Entity* parentEntity = nullptr);
	~PhysicsEntity();

	inline float GetMass()        { return mass; }
	inline float GetRestitution() { return restitution; }
	inline void  SetRestitution(float r) { restitution = r; body->setRestitution(r); }

	// Allows others to do physics to this
	inline btRigidBody* RigidBody() { return body; }

	void SetShape(btCollisionShape* colShape);
	void SetMass(float m);

	void RegisterEnterListener(PhysicsScript* s);
	void RegisterExitListener(PhysicsScript* s);
	void UnregisterEnterListener(PhysicsScript* s);
	void UnregisterExitListener(PhysicsScript* s);

	// Handle collision events
	void CollisionEnter(PhysicsEntity* other);
	void CollisionExit(PhysicsEntity* other);
};

#endif