#include "Material.h"

Material::Material() :
	vertexShader(nullptr),
	pixelShader(nullptr)
{
	// Nothing interesting to do here
}

Material::Material(SimpleVertexShader* v, SimplePixelShader* p, std::vector<std::string> tn, std::vector<std::shared_ptr<SimpleTexture>> tx, ID3D11SamplerState* ss) :
	vertexShader(v),
	pixelShader(p),
	texNames(tn),
	textures(tx),
	sampler(ss)
{
	// Nothing interesting to do here
}

Material::~Material()
{
	// Nothing interesting to do here
}