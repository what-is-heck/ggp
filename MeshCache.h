#pragma once
#pragma comment(lib, "d3d11.lib")
#include <d3d11.h>
#include "ResourceCache.hpp"
#include <DirectXMath.h>
#include <fstream>
#include <unordered_map>

class Mesh;
class Material;
struct Vertex;

class SimpleTexture;

class SimpleVertexShader;
class SimplePixelShader;

// Caches for shaders and textures would be a great addition

class MeshCache : public ResourceCache<Mesh>
{
private:
	ID3D11Device* device;
	std::shared_ptr<Mesh> CreateMesh(std::string name, Vertex* vertices, int vertCount, UINT* indices, int idxCount);
protected:
	std::shared_ptr<Mesh> ImportResource(std::string name) override;
public:
	MeshCache();
	MeshCache(ID3D11Device* device);
	~MeshCache();
};

class TextureCache : public ResourceCache<SimpleTexture>
{
private:
	ID3D11Device* device;
	ID3D11DeviceContext* context;
protected:
	std::shared_ptr<SimpleTexture> ImportResource(std::string name) override;
public:
	TextureCache();
	TextureCache(ID3D11Device* dev, ID3D11DeviceContext* ctx);
	~TextureCache();
};

class MaterialCache : public ResourceCache<Material>
{
private:
	// No need to cache shaders, as they will always all be loaded at program start and all be unloaded at program end
	std::unordered_map<std::string, SimpleVertexShader*> vShaders;
	std::unordered_map<std::string, SimplePixelShader*>  pShaders;

	TextureCache tex;

	ID3D11Device* device;
	ID3D11DeviceContext* context;
	ID3D11SamplerState* sampler;

	enum ShaderType
	{
		VERTEX,
		PIXEL
	};

	void LoadShader(ShaderType type, std::string name, std::wstring file);
protected:
	std::shared_ptr<Material> ImportResource(std::string name) override;
public:
	MaterialCache();
	MaterialCache(ID3D11Device* dev, ID3D11DeviceContext* ctx);
	~MaterialCache();

	inline std::shared_ptr<SimpleTexture> GetTexture(std::string name) { return tex.GetResource(name); }

	void Init();
	void Destroy();
};
