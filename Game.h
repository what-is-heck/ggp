#pragma once

#include "DXCore.h"
#include "SimpleShader.h"
#include "Mesh.h"
#include "Material.h"
#include <DirectXMath.h>
#include <vector>
#include <memory>

#include "MouseState.h"

#include "CameraEntity.h"
#include "MeshEntity.h"
#include "ScriptEntity.h"

#include "MeshCache.h"

#include "Scene.h"
#include "Renderer.h"


class Entity;

class Game : public DXCore
{
private:
	// Keeps track of meshes for us
	MeshCache meshes;
	MaterialCache materials;

	// The basic material for now
	std::shared_ptr<Material> baseMat;

	//The scene, can be a vector or something else if we need more than one
	Scene s;

	//The renderer, handles all of our drawing
	Renderer renderer;

	// All the mesh-entities in the world -- I'll do something more clever later
	std::vector<MeshEntity*> renderables;

	// The current camera being used for rendering
	CameraEntity* primaryCamera;

	// Keep track of everything mouse related for polling from gameobjects
	MouseState mstate;

	// The matrices to go from model space to screen space
	DirectX::XMFLOAT4X4 worldMatrix;
	DirectX::XMFLOAT4X4 viewMatrix;
	DirectX::XMFLOAT4X4 projectionMatrix;

	// TODO : Something cleaner with this
	float time;

	// Keeps track of the old mouse position.  Useful for 
	// determining how far the mouse moved in a single frame.
	POINT prevMousePos;

	friend class Emitter;
public:
	Game(HINSTANCE hInstance);
	~Game();

	// Overridden setup and game loop methods, which
	// will be called automatically
	void Init();
	void OnResize();
	void Update(float deltaTime, float totalTime);
	void Draw(float deltaTime, float totalTime);

	// Overridden mouse input helper methods
	void OnMouseDown (WPARAM buttonState, int x, int y);
	void OnMouseUp	 (WPARAM buttonState, int x, int y);
	void OnMouseMove (WPARAM buttonState, int x, int y);
	void OnMouseWheel(float wheelDelta,   int x, int y);

	inline MouseState* GetMouseState() { return &mstate; }

	inline CameraEntity* GetPrimaryCamera() { return primaryCamera; }
	void SetPrimaryCamera(CameraEntity* c);
};

