#include "Physics.h"

#include "PhysicsEntity.h"

// Globals are gross, but so are object-oriented callbacks in C++
Physics* _physics;
void _PhysicsCallback(btDynamicsWorld* w, btScalar time);

Physics::Physics() :
	colConfig(new btDefaultCollisionConfiguration()),
	colDispatcher(nullptr),
	colBroadphase(new btDbvtBroadphase()),
	solver(new btSequentialImpulseConstraintSolver()),
	world(nullptr),
	entities(),
	collisions(),
	enterEvents(),
	exitEvents()
{
	colDispatcher = new btCollisionDispatcher(colConfig);
	world = new btDiscreteDynamicsWorld(colDispatcher, colBroadphase, solver, colConfig);
	world->setGravity(btVector3(0.0f, -8.0f, 0.0f));
	world->setInternalTickCallback(_PhysicsCallback);

	// This is so gross
	_physics = this;
}

Physics::~Physics()
{
	if (world)
	{
		delete world;
		delete solver;
		delete colBroadphase;
		delete colDispatcher;
		delete colConfig;
	}
}

void Physics::RegisterEntity(PhysicsEntity* e, bool create)
{
	world->addRigidBody(e->body);
	if (create)
	{
		entities.push_back(e);
	}
}

void Physics::UnregisterEntity(PhysicsEntity* e, bool destroy)
{
	for (int i = entities.size() - 1; i >= 0; --i)
	{
		// 300% chance there's a better way to do this but whatever
		if (entities[i] == e)
		{
			world->removeRigidBody(e->body);

			if (destroy)
			{
				// Pop it out quickly (order doesn't matter)
				entities[i] = entities[entities.size() - 1];
				entities.pop_back();
				delete e->shape;
				delete e->body->getMotionState();
				delete e->body;
			}

			break;
		}
	}
}

void Physics::Update(float dt)
{
	for (int i = 0; i < entities.size(); ++i)
	{
		entities[i]->PreStep();
	}

	world->stepSimulation(dt);

	for (int i = 0; i < entities.size(); ++i)
	{
		entities[i]->PostStep();
	}

	for (int i = 0; i < enterEvents.size(); ++i)
	{
		enterEvents[i].first->CollisionEnter(enterEvents[i].second);
		enterEvents[i].second->CollisionEnter(enterEvents[i].first);
	}

	for (int i = 0; i < exitEvents.size(); ++i)
	{
		exitEvents[i].first->CollisionExit(exitEvents[i].second);
		exitEvents[i].second->CollisionExit(exitEvents[i].first);
	}

	enterEvents.clear();
	exitEvents.clear();
}

void Physics::HandleCollisions(btScalar time)
{
	// Number of collision manifolds detected in this tick
	int cc = colDispatcher->getNumManifolds();

	// Collisions recognized in just this tick
	std::vector<ColPair> current;
	
	for (int i = 0; i < cc; ++i)
	{
		btPersistentManifold* contact = colDispatcher->getManifoldByIndexInternal(i);
		const btCollisionObject* ba = contact->getBody0();
		const btCollisionObject* bb = contact->getBody1();
		ColPair pair((PhysicsEntity*)(ba->getUserPointer()), (PhysicsEntity*)(bb->getUserPointer()));

		current.push_back(pair);

		// int found = -1; // -1 indicates that it was not found, anything alse is the index at which it was found
		bool newCollision = true;
		for (int j = 0; j < collisions.size(); ++j)
		{
			if (collisions[j] == pair || collisions[j] == ColPair((PhysicsEntity*)(bb->getUserPointer()), (PhysicsEntity*)(ba->getUserPointer())))
			{
				// found = j;
				newCollision = false;
				break;
			}
		}
		
		// Not already listed in collisions
		// if (found == -1)
		if (newCollision)
		{
			collisions.push_back(pair);

			// DEBUG
			// printf("COLLISION\n");

			// Trigger collision-enter event
			enterEvents.push_back(pair);
		}
	}

	// Collisions that no longer exist (index into collisions)
	std::vector<int> ended;

	// Find all collisions that no longer exist
	for (int i = 0; i < collisions.size(); ++i)
	{
		bool noLongerExists = true;
		for (int j = 0; j < current.size(); ++j)
		{
			if (collisions[i] == current[j])
			{
				noLongerExists = false;
				break;
			}
		}

		if (noLongerExists)
		{
			ended.push_back(i);
		}
	}

	// Get rid of expired collisions
	for (int i = ended.size() - 1; i >= 0; --i)
	{
		// Trigger collision-exit event
		exitEvents.push_back(collisions[ended[i]]);

		// DEBUG
		// printf("EXIT COLLISION\n");

		collisions[ended[i]] = collisions[collisions.size() - 1];
		collisions.pop_back();
	}
}

void _PhysicsCallback(btDynamicsWorld* world, btScalar time)
{
	_physics->HandleCollisions(time);
}