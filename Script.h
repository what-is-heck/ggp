#ifndef SCRIPT_H_
#define SCRIPT_H_

#include "ScriptEntity.h"
#include "MouseState.h"

struct MouseState;

class Script
{
private:
	// ScriptEntity needs to be able to set self at construction, but nothing else should be able to access internals
	friend class ScriptEntity;

	// Points to the ScriptEntity this script is attached to
	ScriptEntity* self;

	// Points to the Game's MouseState
	MouseState* mstate;

	// Allows the ScriptEntity to set itself as the self for this script upon construction
	inline void SetSelf(ScriptEntity* s) { self = s; }

	// Dittom but for the mstate from the Game
	inline void SetMouseState(MouseState* m) { mstate = m; }
protected:
	// Allows scripts, inheriting from this, to access the self property
	inline ScriptEntity* Self() { return self; }

	// Ditto, but for the mouse state
	inline MouseState* GetMouseState() { return mstate; }
public:
	Script();
	virtual ~Script();

	// Event hook for the update loop
	virtual void Update(float dt);
};

#endif