#pragma once
#include "Scene.h"
#include <unordered_map>
#include <typeinfo>
#include "Entity.h"
#include "MeshEntity.h"
#include "ScriptEntity.h"
#include "CameraEntity.h"
#include "PhysicsEntity.h"
#include "ParticleEntity.h"
#include "Physics.h"

#include "MeshCache.h"

class Scene
{
private:
	Game* game;
	std::string name;

	// TODON'T : Get me out of here
	Physics* physics;

	//maps name to entity, names must be unique
	std::unordered_map<std::string, Entity*>  entityMap;

	//maps tags to vectors of entities. entities may have mutliple tags and by default have one for their class.
	std::unordered_map<std::string, std::vector<Entity*>>  tagMap;

	// The basic material for now
	std::shared_ptr<Material> baseMat;

	// Melt material
	std::shared_ptr<Material> meltMat;

	//slight optimization as we don't modify gameobjects much (read: at all [yet])
	std::vector<ScriptEntity*> gameObjects;


	//Pointers to caches used by the game
	MeshCache* meshes;
	MaterialCache* materials;

public:
	Scene();
	Scene(Game* g, std::string filename);
	~Scene();

	void Init(MeshCache* mc, MaterialCache* mtc);
	void Update(float deltaTime, float totalTime);

	template <typename E>
	Entity* SpawnEntity(std::string name) { printf("ERROR : SpawnEntity \""+name+"\" CALLED WITH NON-ENTITY TYPE\n"); return nullptr; }

	template <>
	Entity* SpawnEntity<CameraEntity>(std::string name)
	{
		CameraEntity* e = new CameraEntity(game);

		assert(entityMap.find(name) == entityMap.end() && "entity with name already exists");
		entityMap[name] = e;

		TagObj("class CameraEntity", e);//typeid(CameraEntity).name()

		return e;
	}

	template <>
	Entity* SpawnEntity<MeshEntity>(std::string name)
	{
		MeshEntity* e = new MeshEntity(game, nullptr, baseMat);

		assert(entityMap.find(name) == entityMap.end() && "entity with name already exists");
		entityMap[name] = e;

		TagObj("class MeshEntity", e);

		return e;
	}

	template <>
	Entity* SpawnEntity<ScriptEntity>(std::string name)
	{
		ScriptEntity* e = new ScriptEntity(game);

		assert(entityMap.find(name) == entityMap.end() && "entity with name already exists");
		entityMap[name] = e;

		TagObj("class ScriptEntity", e);

		return e;
	}
	
	template <>
	Entity* SpawnEntity<PhysicsEntity>(std::string name)
	{
		PhysicsEntity* e = new PhysicsEntity(game, physics);

		assert(entityMap.find(name) == entityMap.end() && "entity with name already exists");
		entityMap[name] = e;

		TagObj("class PhysicsEntity", e);

		return e;
	}

	template <>
	Entity* SpawnEntity<ParticleEntity>(std::string name)
	{
		ParticleEntity* e = new ParticleEntity(game);

		assert(entityMap.find(name) == entityMap.end() && "entity with name already exists");
		entityMap[name] = e;

		TagObj("class ParticleEntity", e);

		return e;
	}

	void TagObj(std::string tag, Entity* entity) {
		tagMap.emplace(std::make_pair(tag, std::vector<Entity*>()));
		tagMap.at(tag).push_back(entity);
		//tags will have to be stored in entities if we want to support removal/deletion, 
	}

	std::vector<Entity*> MatchesTag(std::string tag) {
		return tagMap.at(tag);
	}

	template <typename E>
	std::vector<E*> GetAll() { 
		///from http://stackoverflow.com/questions/122316/template-constraints-c
		static_assert(std::is_base_of<Entity, E>::value, "ERROR : ListOf CALLED WITH NON-ENTITY TYPE\n");

		std::string name = typeid(E).name();
		auto v = MatchesTag(name);

		std::vector<E*> u;

		for (std::vector<Entity *>::iterator it = v.begin(); it!=v.end(); it++)
		{
			u.emplace_back(dynamic_cast<E*>(*it));
		}

		return u;
	}
};

