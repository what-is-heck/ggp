#pragma once
#include "SimpleShader.h"
#include "Mesh.h"
#include "Material.h"
#include <DirectXMath.h>
#include <vector>
#include <memory>

#include "Lights.h"

#include "CameraEntity.h"
#include "MeshEntity.h"
#include "ParticleEntity.h"

#include "ShadowMap.h"
#include "Skybox.h"

const size_t MAX_PARTICLES = 0x100000;

class Renderer
{
private:
	std::vector<MeshEntity*> renderables;
	DirectionalLight light;
	DirectionalLight light2;
	CameraEntity* primaryCamera;

	ID3D11RenderTargetView* backBufferRTV;
	ID3D11DepthStencilView* depthStencilView;

	IDXGISwapChain*			swapChain;
	ID3D11Device*			device;
	ID3D11DeviceContext*	context;
	ID3D11SamplerState*		sampler;

	// Particles
	std::vector<ParticleEntity*> emitters;
	ID3D11DepthStencilState*     particleDS;
	ID3D11BlendState*            particleBS;
	ID3D11Buffer*                particleIB;
	SimpleVertexShader*          particleVS;
	SimplePixelShader*           particlePS;

	int width;
	int height;

	//can have multiple of these sometime?
	// Probably yeah
	ShadowMap shadowMap1;
	ShadowMap shadowMap2;

	Skybox skybox;

	friend class Emitter;
public:
	Renderer();
	~Renderer();
	void Init(ID3D11Device* device, ID3D11DeviceContext* context, IDXGISwapChain* swapChain, ID3D11RenderTargetView* backBufferRTV, ID3D11DepthStencilView* depthStencilView);
	void SetCamera(CameraEntity* camera, std::string tag);


	//Someday we might not want to be passing in a vector every time, would make sense to make add/remove methods
	void UpdateRenderables(std::vector<MeshEntity*> renderables);
	void UpdateParticleSystems(std::vector<ParticleEntity*> p);
	void Draw(float deltaTime, float totalTime);
	void RenderShadowMaps();
	void RestoreContext();
	void OnResize(int width, int height);
};

