#ifndef TESTEMITTER_H_
#define TESTEMITTER_H_

#include "Emitter.h"

class TestEmitter : public Emitter
{
private:
	float time;
	float rate;
	float size;

	virtual void Update(float dt);
public:
	TestEmitter();
	TestEmitter(Game* g, std::shared_ptr<SimpleTexture> t, float r, float s);
	virtual ~TestEmitter();
};

#endif