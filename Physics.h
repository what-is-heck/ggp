#ifndef PHYSICS_H_
#define PHYSICS_H_

#define BT_NO_SIMD_OPERATOR_OVERLOADS
#include "btBulletDynamicsCommon.h"

#include <vector>
#include <utility>

#if defined(DEBUG) || defined(_DEBUG)

#pragma comment(lib, "lib/Debug/Bullet2FileLoader_Debug.lib")
#pragma comment(lib, "lib/Debug/Bullet3Collision_Debug.lib")
#pragma comment(lib, "lib/Debug/Bullet3Common_Debug.lib")
#pragma comment(lib, "lib/Debug/Bullet3Dynamics_Debug.lib")
#pragma comment(lib, "lib/Debug/Bullet3Geometry_Debug.lib")
#pragma comment(lib, "lib/Debug/Bullet3OpenCL_clew_Debug.lib")
#pragma comment(lib, "lib/Debug/BulletCollision_Debug.lib")
#pragma comment(lib, "lib/Debug/BulletDynamics_Debug.lib")
#pragma comment(lib, "lib/Debug/BulletInverseDynamics_Debug.lib")
#pragma comment(lib, "lib/Debug/BulletSoftBody_Debug.lib")
#pragma comment(lib, "lib/Debug/LinearMath_Debug.lib")

#else

#pragma comment(lib, "lib/Release/Bullet2FileLoader.lib")
#pragma comment(lib, "lib/Release/Bullet3Collision.lib")
#pragma comment(lib, "lib/Release/Bullet3Common.lib")
#pragma comment(lib, "lib/Release/Bullet3Dynamics.lib")
#pragma comment(lib, "lib/Release/Bullet3Geometry.lib")
#pragma comment(lib, "lib/Release/Bullet3OpenCL_clew.lib")
#pragma comment(lib, "lib/Release/BulletCollision.lib")
#pragma comment(lib, "lib/Release/BulletDynamics.lib")
#pragma comment(lib, "lib/Release/BulletInverseDynamics.lib")
#pragma comment(lib, "lib/Release/BulletSoftBody.lib")
#pragma comment(lib, "lib/Release/LinearMath.lib")

#endif

class PhysicsEntity;

typedef std::pair<PhysicsEntity*, PhysicsEntity*> ColPair;

// Naming is hard
class Physics
{
private:
	btDefaultCollisionConfiguration* colConfig;
	btCollisionDispatcher* colDispatcher;
	btBroadphaseInterface* colBroadphase;
	btSequentialImpulseConstraintSolver* solver;
	
	// The physical world in which simulation occurs
	btDiscreteDynamicsWorld* world;

	// Keep track of all physics entities
	std::vector<PhysicsEntity*> entities;

	// Keep track of currently colliding entities
	std::vector<ColPair> collisions;

	// Collision-enter events to fire
	std::vector<ColPair> enterEvents;

	// Collision-exit events to fire
	std::vector<ColPair> exitEvents;
public:
	Physics();
	~Physics();

	inline btVector3 GetGravity() const { return world->getGravity(); }
	inline void SetGravity(const btVector3& g) { world->setGravity(g); }

	// Adds a PhysicsEntity to the physics simulation, second argument only used when spawning
	void RegisterEntity(PhysicsEntity* e, bool create = false);

	// Removes a PhysicsEntity from the physics simulation, second argument only used when deleting
	void UnregisterEntity(PhysicsEntity* e, bool destroy = false);

	// Runs a tick of the physics simulation with provided delta-time
	void Update(float dt);

	// Handle collisions
	void HandleCollisions(btScalar time);
};

#endif