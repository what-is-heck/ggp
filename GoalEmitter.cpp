#include "GoalEmitter.h"

#include <cstdlib>

void GoalEmitter::Update(float dt)
{
	rtime -= dt;

	if (rtime > 0.0f)
	{
		stime += dt;

		while (stime > 0.001f)
		{
			stime -= 0.001f;

			Particle p;

			p.acc = DirectX::XMVectorSet(0.0f, 2.0f, 0.0f, 1.0f);
			p.vel = DirectX::XMVectorSet(0.0f, (float)(rand()) / (float)RAND_MAX * 2.0f, 0.0f, 1.0f);
			p.pos = DirectX::XMVectorSet((float)(rand()) / (float)RAND_MAX * 8.0f - 4.0f, 0.0f, (float)(rand()) / (float)RAND_MAX * 1.0f - 0.5f, 1.0f);

			p.colBeg = DirectX::XMVectorSet((float)(rand()) / (float)RAND_MAX * 0.4f + 0.4f, (float)(rand()) / (float)RAND_MAX * 0.2f + 0.2f, 0.2f, 1.0f);
			p.colEnd = DirectX::XMVectorSet(0.4f, 0.4f, 0.2f, 1.0f);

			p.sizBeg = (float)(rand()) / (float)RAND_MAX * 0.4f + 0.4f;
			p.sizEnd = 0.01f;
			p.sizCur = p.sizBeg;

			p.time = 0.0f;
			p.life = 1.0f;

			Spawn(p);
		}
	}
}

GoalEmitter::GoalEmitter() :
	ttime(0.0f),
	stime(0.0f),
	rtime(0.0f)
{
	// Nothing interesting to do here
}

GoalEmitter::GoalEmitter(Game* g, std::shared_ptr<SimpleTexture> t, float time) :
	ttime(time),
	stime(0.0f),
	rtime(0.0f),
	Emitter(g, t, 2001)
{
	// Nothing interesting to do here
}

GoalEmitter::~GoalEmitter()
{
	// Nothing interesting to do here
}

void GoalEmitter::Goal()
{
	rtime = ttime;
}