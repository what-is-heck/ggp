#ifndef SIMPLETEXTURE_H_
#define SIMPLETEXTURE_H_

#include <string>
#include <WICTextureLoader.h>

class SimpleTexture
{
private:
	ID3D11ShaderResourceView* r;
public:
	SimpleTexture();
	SimpleTexture(ID3D11Device* device, ID3D11DeviceContext* context, const wchar_t* fp);
	~SimpleTexture();

	inline ID3D11ShaderResourceView* GetSRV()     { return r; }
};

#endif