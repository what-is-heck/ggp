#include "TestEmitter.h"

#include <cstdlib>
#include <cmath>

void TestEmitter::Update(float dt)
{
	time += dt;

	while (time > rate)
	{
		time -= rate;
		
		Particle p;
		/*
		p.acc = DirectX::XMVectorSet((float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, 0.0f);
		p.vel = DirectX::XMVectorSet((float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, 0.0f);
		p.pos = DirectX::XMVectorSet((float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, (float)(rand()) / (float)RAND_MAX * size * 2.0f - 1.0f * size, 0.0f);
		*/

		float u = (float)(rand()) / (float)RAND_MAX;
		float v = (float)(rand()) / (float)RAND_MAX;
		float o1 = 2.0f * 3.14159265359f * u;
		float o2 = acos(2.0f * v - 1.0f);
		float x = size * cos(o1) * sin(o2);
		float y = size * sin(o1) * sin(o2);
		float z = size * cos(o2);

		p.acc = DirectX::XMVectorSet(-x, -y, -z, 1.0f);
		p.vel = DirectX::XMVectorSet(-x, -y, -z, 1.0f);
		p.pos = DirectX::XMVectorSet(x, y, z, 1.0f);

		p.colBeg = DirectX::XMVectorSet(0.8f, 0.2f, 1.0f, 1.0f);
		p.colEnd = DirectX::XMVectorSet(0.2f, 0.2f, 0.2f, 1.0f);

		p.sizBeg = 0.1f;
		p.sizEnd = 0.01f;
		p.sizCur = p.sizBeg;

		p.time = 0.0f;
		p.life = 0.5f;

		Spawn(p);
	}
}

TestEmitter::TestEmitter() :
	time(0.0f),
	rate(1.0f),
	size(1.0f),
	Emitter()
{
	// Nothing interesting to do here
}

TestEmitter::TestEmitter(Game* g, std::shared_ptr<SimpleTexture> t, float r, float s) :
	time(0.0f),
	rate(r),
	size(s),
	Emitter(g, t, size_t(1.0f / r) + 1)
{
	// Nothing interesting to do here
}

TestEmitter::~TestEmitter()
{
	// Nothing interesting to do here
}