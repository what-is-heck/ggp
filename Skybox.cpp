#include "Skybox.h"
#include "DDSTextureLoader.h"


Skybox::Skybox()
{
}


Skybox::~Skybox()
{
	if (initld) {
		skyDepthState->Release();
		skyRastState->Release();
	}
}

void Skybox::init(ID3D11Device* device)
{
	// Create a rasterizer state so we can render backfaces
	D3D11_RASTERIZER_DESC rsDesc = {};
	rsDesc.FillMode = D3D11_FILL_SOLID;
	rsDesc.CullMode = D3D11_CULL_FRONT;
	rsDesc.DepthClipEnable = true;
	device->CreateRasterizerState(&rsDesc, &skyRastState);

	// Create a depth state so that we can accept pixels
	// at a depth less than or EQUAL TO an existing depth
	D3D11_DEPTH_STENCIL_DESC dsDesc = {};
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL; // Make sure we can see the sky (at max depth)
	device->CreateDepthStencilState(&dsDesc, &skyDepthState);
	initld = true;
}

ID3D11DepthStencilState * Skybox::getStencilState()
{
	return skyDepthState;
}

ID3D11RasterizerState * Skybox::getRastState()
{
	return skyRastState;
}
