#ifndef DIRECTIONALLIGHT_H_
#define DIRECTIONALLIGHT_H_

#include <DirectXMath.h>

// TODO : Move this into an entity or something
struct DirectionalLight
{
	DirectX::XMFLOAT4 Ambient;
	DirectX::XMFLOAT4 Diffuse;
	DirectX::XMFLOAT3 Direction;
};

#endif