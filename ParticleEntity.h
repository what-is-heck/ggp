#ifndef PARTICLEENTITY_H_
#define PARTICLEENTITY_H_

#include "Entity.h"

#include <vector>

class Emitter;

class ParticleEntity : public Entity
{
private:
	std::vector<Emitter*> emitters;

	void UpdateEmitters(float dt);

	void RemoveEmitter(Emitter* e);

	friend class Emitter;
	friend class Renderer;
public:
	ParticleEntity();
	ParticleEntity(Game* g, Transform t = Transform(), Entity* p = nullptr);
	~ParticleEntity();

	void AddEmitter(Emitter* e);
};

#endif