#ifndef EMITTER_H_
#define EMITTER_H_

#include "Particle.h"

#include <memory>

class SimplePixelShader;
class SimpleTexture;
class ParticleEntity;
class ID3D11Buffer;
class Game;

class Emitter
{
private:
	// Position as of last update
	DirectX::XMVECTOR ppos;

	// Texture used for rendering particles
	std::shared_ptr<SimpleTexture> texture;

	// Keep a reference to the ParticleEntity this is attached to,
	// to control destruction (if applicable)
	ParticleEntity* owner;

	// Buffers
	ParticleVertex* particleVerts;
	ID3D11Buffer*   vertexBuffer;

	// All particles within the emitter
	Particle* particles;
	size_t count;
	const size_t MAX;

	friend class ParticleEntity;
	friend class Renderer;
protected:
	// Spawns a particle - position in the input particle is
	// the position relative to the emitter
	void Spawn(const Particle& p);

	// Runs an update tick - call Spawn within this
	virtual void Update(float dt);

	// Destroy this emitter
	void Destroy();
public:
	Emitter();
	Emitter(Game* g, std::shared_ptr<SimpleTexture> t, size_t maxParticles);
	Emitter(Emitter&& copy);
	virtual ~Emitter();

	// Updates the particles, and calls Update
	void UpdateInternal(DirectX::XMVECTOR pos, float dt);
};

#endif