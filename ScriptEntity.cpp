#include "ScriptEntity.h"

#include "Script.h"

#include "Game.h"

ScriptEntity::ScriptEntity() :
	script(nullptr),
	Entity()
{
	// Nothing interesting to do here
}

ScriptEntity::ScriptEntity(Game* g, Script* s, const Transform& t, Entity* parentEntity) :
	script(s),
	Entity(g, t, parentEntity)
{
	// script->SetSelf(this);
	// script->SetMouseState(g->GetMouseState());
}

ScriptEntity::~ScriptEntity()
{
	if (script) { delete script; }
}

void ScriptEntity::SetScript(Script* s)
{
	if (script) { delete script; }
	script = s;
	if (s)
	{
		s->SetSelf(this);
		s->SetMouseState(game->GetMouseState());
	}
}

void ScriptEntity::Update(float dt)
{
	if (script) { script->Update(dt); }
}