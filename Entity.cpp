#include "Entity.h"

#include "Game.h"

Entity::Entity() :
	children(),
	parent(nullptr),
	game(nullptr),
	worldMatrix(),
	local(),
	world(),
	needsTransformUpdated(true),
	needsMatrixUpdated(true)
{
	worldMatrix = GetWorldTransform().GetMatrix();
}

Entity::Entity(Game* g, const Transform& t, Entity* parentEntity) :
	children(),
	parent(parentEntity),
	game(g),
	worldMatrix(),
	local(t),
	world(t),
	needsTransformUpdated(true),
	needsMatrixUpdated(true)
{
	if (parent != nullptr)
	{
		parent->AddChild(this);
		world = parent->GetWorldTransform() * local;
	}

	worldMatrix = GetWorldTransform().GetMatrix();
}

Entity::~Entity()
{
	// TODO : Move this to a more sensible location for when entities can come and go freely
	
	size_t cc = children.size();
	for (size_t i = 0; i < cc; ++i)
	{
		children[0]->SetParent(nullptr, true);
	}

	if (parent)
	{
		parent->RemoveChild(this);
	}
}

void Entity::AddChild(Entity* newChild)
{
	children.push_back(newChild);
}

void Entity::RemoveChild(Entity* child)
{
	// Because order doesn't matter, we can zip through and pop it out quickly
	size_t cc = children.size();
	for (size_t i = 0; i < cc; ++i)
	{
		if (children[i] == child)
		{
			children[i] = children[cc - 1];
			children.pop_back();
			return;
		}
	}
}

void Entity::RequireUpdate()
{
	needsTransformUpdated = true;
	needsMatrixUpdated = true;

	// Recursively require updates in all children, as a change in this affects them
	size_t cc = children.size();
	for (size_t i = 0; i < cc; ++i)
	{
		children[i]->RequireUpdate();
	}
}

Transform Entity::GetWorldTransform()
{
	if (parent != nullptr)
	{
		if (needsTransformUpdated)
		{
			world = parent->GetWorldTransform() * local;
			needsTransformUpdated = false;
		}

		return world;
	}
	else
	{
		// The local transform is the also technically the world transform when there is no parent
		// This bypasses unnecessary work
		return local;
	}
}

DirectX::XMFLOAT4X4 Entity::GetWorldMatrix()
{
	if (needsMatrixUpdated)
	{
		// worldMatrix = GetWorldTransform().GetMatrix();
		DirectX::XMStoreFloat4x4(&worldMatrix, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&(GetWorldTransform().GetMatrix()))));
		needsMatrixUpdated = false;
	}

	return worldMatrix;
}

void Entity::SetParent(Entity* newParent, bool keepWorldTransform)
{
	Transform currWorld = GetWorldTransform();

	if (parent) { parent->RemoveChild(this); }
	parent = newParent;
	if (parent) { parent->AddChild(this); }

	if (keepWorldTransform)
	{
		SetPositionWorld(currWorld.GetPosition());
		SetRotationWorld(currWorld.GetRotation());
	}

	RequireUpdate();
}

void Entity::SetPosition(DirectX::XMVECTOR v)
{
	local.SetPosition(v);

	RequireUpdate();
}

void Entity::SetPositionLocal(DirectX::XMVECTOR v)
{
	local.SetPosition(DirectX::XMVector3Rotate(v, local.GetRotation()));

	RequireUpdate();
}

void Entity::SetPositionWorld(DirectX::XMVECTOR v)
{
	Transform w = GetWorldTransform();

	local.TranslateLocal(DirectX::XMVector3Rotate(DirectX::XMVectorSubtract(v, w.GetPosition()), DirectX::XMQuaternionConjugate(w.GetRotation())));

	RequireUpdate();
}

void Entity::SetRotation(DirectX::XMVECTOR q)
{
	local.SetRotation(q);

	RequireUpdate();
}

void Entity::SetRotationWorld(DirectX::XMVECTOR q)
{
	local.RotateLocal(DirectX::XMQuaternionMultiply(q, DirectX::XMQuaternionConjugate(GetWorldTransform().GetRotation())));
	local.SetRotation(DirectX::XMQuaternionNormalize(local.GetRotation()));

	RequireUpdate();
}

void Entity::SetScale(DirectX::XMVECTOR v)
{
	local.SetScale(v);

	// Because scale doesn't pass down, there is no need to require a full update
	needsMatrixUpdated = true;
	needsTransformUpdated = true;
}

void Entity::Translate(DirectX::XMVECTOR v)
{
	local.TranslateGlobal(v);

	RequireUpdate();
}

void Entity::TranslateLocal(DirectX::XMVECTOR v)
{
	local.TranslateLocal(v);

	RequireUpdate();
}

void Entity::TranslateWorld(DirectX::XMVECTOR v)
{
	local.TranslateLocal(DirectX::XMVector3Rotate(v, DirectX::XMQuaternionConjugate(GetWorldTransform().GetRotation())));

	RequireUpdate();
}

void Entity::Rotate(DirectX::XMVECTOR q)
{
	local.RotateGlobal(q);

	RequireUpdate();
}

void Entity::RotateLocal(DirectX::XMVECTOR q)
{
	local.RotateLocal(q);

	RequireUpdate();
}

void Entity::RotateWorld(DirectX::XMVECTOR q)
{
	DirectX::XMVECTOR rl = GetTransform().GetRotation();
	DirectX::XMVECTOR ri = DirectX::XMQuaternionConjugate(rl);

	DirectX::XMVECTOR r = DirectX::XMQuaternionMultiply(rl, DirectX::XMQuaternionMultiply(q, ri));
	r = DirectX::XMQuaternionNormalize(r);

	local.RotateLocal(r);

	RequireUpdate();
}

void Entity::Scale(DirectX::XMVECTOR v)
{
	local.ScaleMul(v);

	RequireUpdate();
}