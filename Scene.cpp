#pragma once
#include "Scene.h"
#include "Scripts/CameraScript.h"
#include "Scripts/MeltScript.h"
#include "Scripts/RotatorScript.h"
#include "TestEmitter.h"
#include "GoalEmitter.h"
#include <string>
#include <cstdlib>
#include <ctime>

Scene::Scene(Game * g, std::string filename)
{
	game = g;
	//we don't do anything with filename yet, there's some hardcoded objects in init
	this->name = filename;
	physics = nullptr;
}

Scene::Scene()
{
}

Scene::~Scene()
{
	if (entityMap.size() != 0) {
		for (std::unordered_map<std::string, Entity*>::iterator i = entityMap.begin(); i != entityMap.end(); ++i) {
			delete i->second;
		}
		entityMap.clear();
	}
	if (entityMap.size() != 0) {
		for (std::unordered_map<std::string, std::vector<Entity*>>::iterator i = tagMap.begin(); i != tagMap.end(); ++i) {
			i->second.clear();
		}
		tagMap.clear();
	}

	if (physics) { delete physics; }
}

void Scene::Update(float deltaTime, float totalTime)
{
	float s = sin(totalTime);

	// entityMap["m4"]->SetRotation(DirectX::XMQuaternionRotationRollPitchYaw(0.0f, s, 0.0f));

	//this is cached and needs to be updated.
	for (size_t i = 0; i < gameObjects.size(); ++i)
	{
		gameObjects[i]->Update(deltaTime);
	}

	physics->Update(deltaTime);
}

void Scene::Init(MeshCache* mc, MaterialCache* mtc)
{
	srand(time(nullptr));

	physics = new Physics();

	//Save our refs
	meshes = mc;
	materials = mtc;

	baseMat = materials->GetResource("Assets/Materials/basic.mat");
	//meltMat = materials->GetResource("Assets/Materials/melt.mat");
	// Populate the scene

	// BEGIN TABLE-TOP
	PhysicsEntity* table0 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table0");
	table0->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table0->SetPosition(DirectX::XMVectorSet(0.0f, -14.0f, -8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table0->RigidBody()->setRestitution(0.2f);

	MeshEntity* tablem0 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem0");
	tablem0->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem0->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem0->SetParent(table0, false);

	PhysicsEntity* table1 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table1");
	table1->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table1->SetPosition(DirectX::XMVectorSet(0.0f, -14.0f, +8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table1->RigidBody()->setRestitution(0.2f);

	MeshEntity* tablem1 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem1");
	tablem1->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem1->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem1->SetParent(table1, false);

	PhysicsEntity* table2 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table2");
	table2->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table2->SetPosition(DirectX::XMVectorSet(+16.0f, -10.0f, +8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table2->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem2 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem2");
	tablem2->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem2->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem2->SetParent(table2, false);

	PhysicsEntity* table3 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table3");
	table3->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table3->SetPosition(DirectX::XMVectorSet(-16.0f, -10.0f, +8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table3->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem3 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem3");
	tablem3->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem3->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem3->SetParent(table3, false);

	PhysicsEntity* table4 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table4");
	table4->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table4->SetPosition(DirectX::XMVectorSet(+16.0f, -10.0f, -8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table4->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem4 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem4");
	tablem4->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem4->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem4->SetParent(table4, false);

	PhysicsEntity* table5 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table5");
	table5->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table5->SetPosition(DirectX::XMVectorSet(-16.0f, -10.0f, -8.0f, 1.0f));
	// p0->SetParent(m4, false);
	table5->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem5 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem5");
	tablem5->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem5->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem5->SetParent(table5, false);



	PhysicsEntity* table6 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table6");
	table6->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table6->SetPosition(DirectX::XMVectorSet(+12.0f, -10.0f, -24.0f, 1.0f));
	// p0->SetParent(m4, false);
	table6->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem6 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem6");
	tablem6->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem6->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem6->SetParent(table6, false);

	PhysicsEntity* table7 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table7");
	table7->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table7->SetPosition(DirectX::XMVectorSet(-12.0f, -10.0f, -24.0f, 1.0f));
	// p0->SetParent(m4, false);
	table7->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem7 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem7");
	tablem7->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem7->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem7->SetParent(table7, false);

	PhysicsEntity* table8 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table8");
	table8->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table8->SetPosition(DirectX::XMVectorSet(+12.0f, -10.0f, +24.0f, 1.0f));
	// p0->SetParent(m4, false);
	table8->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem8 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem8");
	tablem8->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem8->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem8->SetParent(table8, false);

	PhysicsEntity* table9 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("table9");
	table9->SetShape(new btBoxShape(btVector3(8.0f, 8.0f, 8.0f)));
	table9->SetPosition(DirectX::XMVectorSet(-12.0f, -10.0f, +24.0f, 1.0f));
	// p0->SetParent(m4, false);
	table9->RigidBody()->setRestitution(0.8f);

	MeshEntity* tablem9 = (MeshEntity*)SpawnEntity<MeshEntity>("tablem9");
	tablem9->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	tablem9->SetScale(DirectX::XMVectorSet(16.0f, 16.0f, 16.0f, 1.0f));
	tablem9->SetParent(table9, false);
	// END TABLE-TOP

	// BEGIN BALL
	PhysicsEntity* p1 = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("p1");
	p1->SetPosition(DirectX::XMVectorSet(4.0f, 6.0f, 0.0f, 1.0f));
	p1->SetMass(1.0f);
	p1->SetShape(new btSphereShape(0.5f));
	p1->RigidBody()->setRestitution(1.0f);
	/*
	MeshEntity* m6 = (MeshEntity*)SpawnEntity<MeshEntity>("m6");
	m6->SetMesh(meshes->GetResource("Assets/Meshes/sphere.obj"));
	m6->SetMaterial(materials->GetResource("Assets/Materials/poolball1.mat"));
	m6->SetParent(p1, false);
	*/
	ParticleEntity* m6 = (ParticleEntity*)SpawnEntity<ParticleEntity>("m6");
	m6->AddEmitter(new TestEmitter(game, materials->GetTexture("Assets/Textures/particle.png"), 0.0002f, 0.6f));
	m6->SetParent(p1, false);
	// END BALL

	ScriptEntity* cs = (ScriptEntity*)SpawnEntity<ScriptEntity>("cs");
	cs->SetScript(new CameraScript(p1));
	// cs->SetPosition(DirectX::XMVectorSet(0, 0, -10, 0));
	// cs->SetParent(p1, false);

	CameraEntity* c = (CameraEntity*)SpawnEntity<CameraEntity>("c");
	c->SetParent(cs, false);
	c->SetPosition(DirectX::XMVectorSet(0.0f, 0.0f, -6.0f, 0.0f));
	TagObj("MainCamera", c);

	// BEGIN GOALS

	ParticleEntity* goal1 = (ParticleEntity*)SpawnEntity<ParticleEntity>("goal1");
	GoalEmitter* ge1 = new GoalEmitter(game, materials->GetTexture("Assets/Textures/particle.png"), 2.0f);
	goal1->AddEmitter(ge1);
	goal1->SetPosition(DirectX::XMVectorSet(0.0f, -6.0f, -15.0f, 0.0f));
	ParticleEntity* goal2 = (ParticleEntity*)SpawnEntity<ParticleEntity>("goal2");
	GoalEmitter* ge2 = new GoalEmitter(game, materials->GetTexture("Assets/Textures/particle.png"), 2.0f);
	goal2->AddEmitter(ge2);
	goal2->SetPosition(DirectX::XMVectorSet(0.0f, -6.0f, +15.0f, 0.0f));

	// END GOALS

	const int BALL_COUNT = 15;
	PhysicsEntity* px[BALL_COUNT];
	MeshEntity* bx[BALL_COUNT];
	for (int i = 0; i < BALL_COUNT; ++i)
	{
		px[i] = (PhysicsEntity*)SpawnEntity<PhysicsEntity>("px" + std::to_string(i));
		px[i]->SetPosition(DirectX::XMVectorSet(((float)(rand()) / (float)(RAND_MAX) - 0.5f) * 12.0f, 4.0f + ((float)(rand()) / (float)(RAND_MAX)-0.5f) * 4.0f, ((float)(rand()) / (float)(RAND_MAX)-0.5f) * 12.0f, 1.0f));
		px[i]->SetMass(1.0f);
		px[i]->SetShape(new btSphereShape(0.5f));
		px[i]->RigidBody()->setRestitution(1.0f);
		
		
		bx[i] = (MeshEntity*)SpawnEntity<MeshEntity>("bx" + std::to_string(i));
		bx[i]->SetMesh(meshes->GetResource("Assets/Meshes/sphere.obj"));
		bx[i]->SetMaterial(materials->GetResource("Assets/Materials/poolball"+ std::to_string(i+1) +".mat"));
		bx[i]->SetParent(px[i], false);

		ScriptEntity* temp = (ScriptEntity*)SpawnEntity<ScriptEntity>("melt"+ std::to_string(i));
		temp->SetScript(new MeltScript(ge1, ge2));
		temp->SetParent(bx[i], false);
		
		/*
		ParticleEntity* temp = (ParticleEntity*)SpawnEntity<ParticleEntity>("bx" + std::to_string(i));
		temp->AddEmitter(new TestEmitter(game, materials->GetTexture("Assets/Textures/Particle.png"), 0.0004f, 0.5f));
		temp->SetParent(px[i], false);
		*/
	}

	//WARNING! Renderer counts on skybox being last in order! Don't put anything after unless you refactor it!
	MeshEntity* skyBox = (MeshEntity*)SpawnEntity<MeshEntity>("skybox");
	skyBox->SetMesh(meshes->GetResource("Assets/Meshes/cube.obj"));
	skyBox->SetMaterial(materials->GetResource("Assets/Materials/skybox.mat"));

	gameObjects = GetAll<ScriptEntity>();
}
