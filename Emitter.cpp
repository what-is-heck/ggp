#include "Emitter.h"

#include <cstdio>
#include <d3d11.h>
#include <DirectXMath.h>

#include "ParticleEntity.h"
#include "Game.h"

void Emitter::Spawn(const Particle& p)
{
	if (count < MAX)
	{
		particles[count] = p;
		particles[count].pos = DirectX::XMVectorAdd(particles[count].pos, ppos);
		count += 1;
	}
	else
	{
		printf("ERROR : Particle spawn exceeds maximum particle count\n");
	}
}

void Emitter::Update(float dt)
{
	// Nothing interesting to do here
}

void Emitter::Destroy()
{
	owner->RemoveEmitter(this);
}

Emitter::Emitter() :
	ppos(),
	texture(),
	owner(nullptr),
	particleVerts(nullptr),
	vertexBuffer(nullptr),
	particles(nullptr),
	count(0),
	MAX(0)
{
	// Nothing interesting to do here
}

Emitter::Emitter(Game* g, std::shared_ptr<SimpleTexture> t, size_t maxParticles) :
	ppos(),
	texture(t),
	owner(nullptr),
	particleVerts(nullptr),
	vertexBuffer(nullptr),
	particles(nullptr),
	count(0),
	MAX(maxParticles)
{
	particles = new Particle[MAX];

	// Create local particle vertices (easier to update)
	// Do UV's here, as those will never change
	particleVerts = new ParticleVertex[4 * maxParticles];
	for (int i = 0; i < maxParticles * 4; i += 4)
	{
		particleVerts[i + 0].UV = DirectX::XMFLOAT2(0, 0);
		particleVerts[i + 1].UV = DirectX::XMFLOAT2(1, 0);
		particleVerts[i + 2].UV = DirectX::XMFLOAT2(1, 1);
		particleVerts[i + 3].UV = DirectX::XMFLOAT2(0, 1);
	}

	// DYNAMIC vertex buffer (no initial data necessary)
	D3D11_BUFFER_DESC vbDesc = {};
	vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vbDesc.Usage = D3D11_USAGE_DYNAMIC;
	vbDesc.ByteWidth = sizeof(ParticleVertex) * 4 * maxParticles;
	g->renderer.device->CreateBuffer(&vbDesc, 0, &vertexBuffer);
}

Emitter::Emitter(Emitter&& copy) :
	ppos(copy.ppos),
	texture(copy.texture),
	owner(copy.owner),
	particleVerts(copy.particleVerts),
	vertexBuffer(copy.vertexBuffer),
	particles(copy.particles),
	count(copy.count),
	MAX(copy.MAX)
{
	copy.owner = nullptr;
	copy.particleVerts = nullptr;
	copy.vertexBuffer = nullptr;
	copy.particles = nullptr;
}

Emitter::~Emitter()
{
	if (particles)
	{
		delete[] particles;
		delete particleVerts;
		vertexBuffer->Release();
	}
}

void Emitter::UpdateInternal(DirectX::XMVECTOR pos, float dt)
{
	ppos = pos;
	Update(dt);

	// Number of particles that have expired
	size_t expired = 0;

	for (int i = count - 1; i >= 0; --i)
	{
		particles[i].time += dt;

		// If the particle has expired, quickly pop it out
		if (particles[i].time < particles[i].life)
		{
			particles[i].vel = DirectX::XMVectorAdd(DirectX::XMVectorMultiply(particles[i].acc, DirectX::XMVectorSet(dt, dt, dt, 0.0f)), particles[i].vel);
			particles[i].pos = DirectX::XMVectorAdd(DirectX::XMVectorMultiply(particles[i].vel, DirectX::XMVectorSet(dt, dt, dt, 0.0f)), particles[i].pos);

			particles[i].sizCur = particles[i].sizBeg * (1.0f - particles[i].time / particles[i].life) +
			                      particles[i].sizEnd * (particles[i].time / particles[i].life);
		}
		else
		{
			expired += 1;
			particles[i] = particles[count - expired];
		}
	}

	count -= expired;
}