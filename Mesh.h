#ifndef MESH_H_
#define MESH_H_

#include <cstdint>

struct Vertex;
struct ID3D11Buffer;
struct ID3D11Device;

class Mesh
{
private:
	ID3D11Buffer* vbuf;
	ID3D11Buffer* ibuf;

	uint32_t icnt;

	void CreateBuffers(Vertex* verts, uint32_t vertCount, uint32_t* indices, uint32_t indCount, ID3D11Device* device);

	friend class MeshCache;
public:
	Mesh();
	Mesh(const Mesh* copy) = delete;
	Mesh(Mesh&& copy);
	Mesh(Vertex* verts, uint32_t vertCount, uint32_t* indices, uint32_t indCount, ID3D11Device* device);
	Mesh(const char* fp, ID3D11Device* device);
	~Mesh();

	Mesh& operator= (Mesh&& copy);
	Mesh& operator= (const Mesh& copy) = delete;

	inline ID3D11Buffer** GetVertexBuffer() { return &vbuf; }
	inline ID3D11Buffer** GetIndexBuffer() { return &ibuf; }
	inline uint32_t GetIndexCount() { return icnt; }
};

#endif