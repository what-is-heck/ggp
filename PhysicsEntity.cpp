#include "PhysicsEntity.h"

#include <DirectXMath.h>

#include "Physics.h"
#include "Scripts/PhysicsScript.h"

PhysicsEntity::PhysicsEntity() :
	body(nullptr),
	shape(nullptr),
	physics(nullptr),
	enterListeners(),
	exitListeners(),
	mass(0.0f),
	restitution(0.0f),
	Entity()
{
	// Nothing interesting to do here (yet)
}

PhysicsEntity::PhysicsEntity(Game* g, Physics* p, const Transform& t, Entity* parentEntity) :
	body(nullptr),
	shape(new btSphereShape(1.0f)),
	physics(p),
	enterListeners(),
	exitListeners(),
	mass(0.0f),
	restitution(0.0f),
	Entity(g, t, parentEntity)
{
	UpdateRigidBody();
}

PhysicsEntity::~PhysicsEntity()
{
	physics->UnregisterEntity(this, true);
}

void PhysicsEntity::PreStep()
{
	// Move the physics body to match the game-entity body
	Transform t = GetWorldTransform();

	DirectX::XMFLOAT4A pos;
	DirectX::XMFLOAT4A rot;

	DirectX::XMStoreFloat4A(&pos, t.GetPosition());
	DirectX::XMStoreFloat4A(&rot, t.GetRotation());

	btTransform gt(btQuaternion(rot.x, rot.y, rot.z, rot.w), btVector3(pos.x, pos.y, pos.z));

	body->setWorldTransform(gt);
	body->getMotionState()->setWorldTransform(gt);
}

void PhysicsEntity::PostStep()
{
	// Now move the game-entity body to match the physics body
	btTransform  t   = body->getWorldTransform();
	btQuaternion rot = t.getRotation();
	btVector3    pos = t.getOrigin();

	SetPositionWorld(DirectX::XMVectorSet(pos.x(), pos.y(), pos.z(), 1.0f));
	SetRotationWorld(DirectX::XMVectorSet(rot.x(), rot.y(), rot.z(), rot.w()));
}

void PhysicsEntity::UpdateRigidBody()
{
	bool exists = body;
	if (exists)
	{
		physics->UnregisterEntity(this, false);

		delete body->getMotionState();
		delete body;
	}

	Transform t = GetWorldTransform();
	DirectX::XMFLOAT4A pos;
	DirectX::XMFLOAT4A rot;
	DirectX::XMStoreFloat4A(&pos, t.GetPosition());
	DirectX::XMStoreFloat4A(&rot, t.GetRotation());

	btTransform gt(btQuaternion(rot.x, rot.y, rot.z, rot.w), btVector3(pos.x, pos.y, pos.z));
	btDefaultMotionState* mstate = new btDefaultMotionState(gt);

	btVector3 inertia;
	if (mass != 0.0f) { shape->calculateLocalInertia(mass, inertia); }

	btRigidBody::btRigidBodyConstructionInfo info(mass, mstate, shape, inertia);
	body = new btRigidBody(info);
	body->setRestitution(restitution);
	body->setUserPointer(this);

	physics->RegisterEntity(this, !exists);
}

void PhysicsEntity::SetShape(btCollisionShape* colShape)
{
	if (shape) { delete shape; }
	shape = colShape;

	UpdateRigidBody();
}

void PhysicsEntity::SetMass(float m)
{
	mass = m;

	UpdateRigidBody();
}

void PhysicsEntity::RegisterEnterListener(PhysicsScript* s)
{
	enterListeners.push_back(s);
}

void PhysicsEntity::RegisterExitListener(PhysicsScript* s)
{
	exitListeners.push_back(s);
}

void PhysicsEntity::UnregisterEnterListener(PhysicsScript* s)
{
	for (int i = 0; i < enterListeners.size(); ++i)
	{
		if (enterListeners[i] == s)
		{
			enterListeners[i] = enterListeners[enterListeners.size() - 1];
			enterListeners.pop_back();
			break;
		}
	}
}

void PhysicsEntity::UnregisterExitListener(PhysicsScript* s)
{
	for (int i = 0; i < exitListeners.size(); ++i)
	{
		if (exitListeners[i] == s)
		{
			exitListeners[i] = exitListeners[exitListeners.size() - 1];
			exitListeners.pop_back();
			break;
		}
	}
}

void PhysicsEntity::CollisionEnter(PhysicsEntity* other)
{
	for (int i = 0; i < enterListeners.size(); ++i)
	{
		enterListeners[i]->CollisionEnter(other);
	}
	
}

void PhysicsEntity::CollisionExit(PhysicsEntity* other)
{
	for (int i = 0; i < exitListeners.size(); ++i)
	{
		enterListeners[i]->CollisionExit(other);
	}
}