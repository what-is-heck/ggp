#ifndef MOUSESTATE_H_
#define MOUSESTATE_H_

#include <cstdint>

struct MouseState
{
	// Wheel movement since last update
	float dw;

	// Current position
	int16_t x;
	int16_t y;

	// Change in mouse position since last update
	int16_t dx;
	int16_t dy;

	// Mouse buttons currently pressed
	bool lmb;
	bool rmb;

	// Mouse buttons pressed last update
	bool plmb;
	bool prmb;
};

#endif