#ifndef GOALEMITTER_H_
#define GOALEMITTER_H_

#include "Emitter.h"

class GoalEmitter : public Emitter
{
private:
	float ttime; // Total time of spawning remaining
	float stime; // Time to next particle spawn
	float rtime; // Time spent spawning

	virtual void Update(float dt);
public:
	GoalEmitter();
	GoalEmitter(Game* g, std::shared_ptr<SimpleTexture> t, float time);
	virtual ~GoalEmitter();

	void Goal();
};

#endif