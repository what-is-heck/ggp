cbuffer externalData : register(b0)
{
	matrix view;
	matrix projection;
};

// Describes individual vertex data
struct VertexShaderInput
{
	float3 position : POSITION;
	float2 uv       : TEXCOORD;
	float4 color    : COLOR;
	float size      : SIZE;
};

// Defines the output data of our vertex shader
struct VertexToPixel
{
	float4 position : SV_POSITION;
	float2 uv       : TEXCOORD0;
	float4 color    : TEXCOORD1;
};

VertexToPixel main( VertexShaderInput input )
{
	// Set up output struct
	VertexToPixel output;

	// Calculate position
	output.position = mul(float4(input.position, 1.0f), mul(view, projection));

	// Offset position based on UV
	float2 offset = (input.uv * 2.0 - 1.0) * input.size;
	offset.x *= 0.5625;
	offset.y *= -1;
	output.position.xy += offset;

	output.uv = input.uv;
	output.color = input.color;

	return output;
}