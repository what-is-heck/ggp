#include "ParticleEntity.h"

#include "Emitter.h"

void ParticleEntity::UpdateEmitters(float dt)
{
	for (size_t i = 0; i < emitters.size(); ++i)
	{
		emitters[i]->UpdateInternal(GetWorldTransform().GetPosition(), dt);
	}
}

void ParticleEntity::RemoveEmitter(Emitter* e)
{
	for (size_t i = emitters.size() - 1; i >= 0; --i)
	{
		if (emitters[i] == e)
		{
			delete e;
			emitters[i] = emitters[emitters.size() - 1];
			emitters.pop_back();

			break;
		}
	}
}

ParticleEntity::ParticleEntity() :
	emitters(),
	Entity()
{
	// Nothing interesting to do here
}

ParticleEntity::ParticleEntity(Game* g, Transform t, Entity* p) :
	emitters(),
	Entity(g, t, p)
{
	// Nothing interesting to do here
}

ParticleEntity::~ParticleEntity()
{
	for (size_t i = 0; i < emitters.size(); ++i)
	{
		delete emitters[i];
	}
}

void ParticleEntity::AddEmitter(Emitter* e)
{
	e->owner = this;
	emitters.push_back(e);
}