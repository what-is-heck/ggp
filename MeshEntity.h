#ifndef MESHENTITY_H_
#define MESHENTITY_H_

#include "Entity.h"

#include <memory>

class Mesh;
class Material;

class MeshEntity : public Entity
{
private:
	std::shared_ptr<Mesh> mesh;
	std::shared_ptr<Material> material;
public:
	MeshEntity();
	MeshEntity(Game* g, std::shared_ptr<Mesh> m, std::shared_ptr<Material> mat, const Transform& t = Transform(), Entity* parentEntity = nullptr);
	virtual ~MeshEntity();

	inline std::shared_ptr<Mesh> GetMesh() { return mesh; }
	inline void  SetMesh(std::shared_ptr<Mesh> m) { mesh = m; }

	inline std::shared_ptr<Material> GetMaterial() { return material; }
	inline void SetMaterial(std::shared_ptr<Material> m) { material = m; }
};

#endif