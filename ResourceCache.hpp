#include <memory>
#include <unordered_map>

template <class T> class ResourceCache
{
private:
	std::unordered_map<std::string, std::weak_ptr<T>> map;
protected:
	void RegisterResource(std::string name, std::shared_ptr<T>* shared_resource) { map[name] = *shared_resource; };
	virtual std::shared_ptr<T> ImportResource(std::string name) { return nullptr; };
public:
	std::shared_ptr<T> GetResource(std::string name) {
		if (map.count(name) > 0)
			if (auto shared_ptr = map.at(name).lock())
				return shared_ptr;
		return ImportResource(name);
	};
	ResourceCache(void) { this->map = std::unordered_map<std::string, std::weak_ptr<T>>(); };
};