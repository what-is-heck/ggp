#include "ShadowMap.h"
#include "SimpleShader.h"

#include "Lights.h"

ShadowMap::ShadowMap()
{
	//I don't zero everything because im lazy.
	shadowDSV = 0;
}

ShadowMap::ShadowMap(int size, DirectionalLight* light)
{
	shadowDSV = 0;
	shadowMapSize = size;
	this->light = light;
}


ShadowMap::~ShadowMap()
{
	// Clean up shadow map
	if (shadowDSV!=0) {
		shadowDSV->Release();
		shadowSRV->Release();
		shadowRasterizer->Release();
		shadowSampler->Release();
		delete shadowVS;
	}
}

void ShadowMap::Init(ID3D11Device* device, ID3D11DeviceContext* context)
{
	RecalculateMatrices();
	/*BELOW: Literally just copy and paste example code*/
	
	// Create the actual texture that will be the shadow map
	D3D11_TEXTURE2D_DESC shadowDesc = {};
	shadowDesc.Width = shadowMapSize;
	shadowDesc.Height = shadowMapSize;
	shadowDesc.ArraySize = 1;
	shadowDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE;
	shadowDesc.CPUAccessFlags = 0;
	shadowDesc.Format = DXGI_FORMAT_R32_TYPELESS;
	shadowDesc.MipLevels = 1;
	shadowDesc.MiscFlags = 0;
	shadowDesc.SampleDesc.Count = 1;
	shadowDesc.SampleDesc.Quality = 0;
	shadowDesc.Usage = D3D11_USAGE_DEFAULT;
	ID3D11Texture2D* shadowTexture;
	device->CreateTexture2D(&shadowDesc, 0, &shadowTexture);

	// Create the depth/stencil
	D3D11_DEPTH_STENCIL_VIEW_DESC shadowDSDesc = {};
	shadowDSDesc.Format = DXGI_FORMAT_D32_FLOAT;
	shadowDSDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	shadowDSDesc.Texture2D.MipSlice = 0;
	device->CreateDepthStencilView(shadowTexture, &shadowDSDesc, &shadowDSV);

	// Create the SRV for the shadow map
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	device->CreateShaderResourceView(shadowTexture, &srvDesc, &shadowSRV);

	// Release the texture reference since we don't need it
	shadowTexture->Release();

	// Create the special "comparison" sampler state for shadows
	D3D11_SAMPLER_DESC shadowSampDesc = {};
	shadowSampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR; // Could be anisotropic
	shadowSampDesc.ComparisonFunc = D3D11_COMPARISON_LESS;
	shadowSampDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	shadowSampDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	shadowSampDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	shadowSampDesc.BorderColor[0] = 1.0f;
	shadowSampDesc.BorderColor[1] = 1.0f;
	shadowSampDesc.BorderColor[2] = 1.0f;
	shadowSampDesc.BorderColor[3] = 1.0f;
	device->CreateSamplerState(&shadowSampDesc, &shadowSampler);

	// Create a rasterizer state
	D3D11_RASTERIZER_DESC shadowRastDesc = {};
	shadowRastDesc.FillMode = D3D11_FILL_SOLID;
	shadowRastDesc.CullMode = D3D11_CULL_BACK;
	shadowRastDesc.DepthClipEnable = true;
	shadowRastDesc.DepthBias = 1000; // Multiplied by (smallest possible value > 0 in depth buffer)
	shadowRastDesc.DepthBiasClamp = 0.0f;
	shadowRastDesc.SlopeScaledDepthBias = 1.0f;
	device->CreateRasterizerState(&shadowRastDesc, &shadowRasterizer);

	//TODO: Shadercache, shared ptr
	shadowVS = new SimpleVertexShader(device, context);
	if (!shadowVS->LoadShaderFile(L"x64/Debug/ShadowVS.cso"))
		shadowVS->LoadShaderFile(L"ShadowVS.cso");
}

SimpleVertexShader * ShadowMap::GetShadowVS()
{
	return shadowVS;
}

void ShadowMap::PrepareContext(ID3D11DeviceContext * context)
{
	// Set up the render targets and depth buffer (shadow map) and
	// other pipeline states
	context->OMSetRenderTargets(0, 0, shadowDSV);
	context->ClearDepthStencilView(shadowDSV, D3D11_CLEAR_DEPTH, 1.0f, 0);
	context->RSSetState(shadowRasterizer);

	D3D11_VIEWPORT vp = {};
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.Width = (float)shadowMapSize;
	vp.Height = (float)shadowMapSize;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	context->RSSetViewports(1, &vp);
}

DirectX::XMFLOAT4X4 ShadowMap::GetViewMatrix()
{
	return shadowViewMatrix;
}

DirectX::XMFLOAT4X4 ShadowMap::GetProjectionMatrix()
{
	return shadowProjectionMatrix;
}

ID3D11SamplerState * ShadowMap::GetSampler()
{
	return shadowSampler;
}

ID3D11ShaderResourceView * ShadowMap::GetSRV()
{
	return shadowSRV;
}

void ShadowMap::RecalculateMatrices()
{
	DirectX::XMMATRIX shView = DirectX::XMMatrixLookAtLH(
		DirectX::XMVectorScale(DirectX::XMLoadFloat3(&light->Direction), -10), // Eye position
		DirectX::XMVectorSet(0, 0, 0, 0),	// Look at pos
		DirectX::XMVectorSet(0, 1, 0, 0));	// Up vector
	XMStoreFloat4x4(&shadowViewMatrix, XMMatrixTranspose(shView));

	//this should be based on the scene bounds/camera frustrum
	DirectX::XMMATRIX shProj = DirectX::XMMatrixOrthographicLH(
		50.0f,		// Width
		50.0f,		// Height
		0.1f,		// Near clip
		100.0f);	// Far clip

					//shProj = DirectX::XMMatrixMultiply(DirectX::XMMatrixTranslation(0,-1,-3),shProj);
	XMStoreFloat4x4(&shadowProjectionMatrix, DirectX::XMMatrixTranspose(shProj));
}
