#pragma once
#include <DirectXMath.h>
#include <d3d11.h>

class SimpleVertexShader;
class DirectionalLight;
class CameraEntity;

class ShadowMap
{
private:
	// Shadow stuff
	int shadowMapSize;
	ID3D11DepthStencilView* shadowDSV;
	ID3D11ShaderResourceView* shadowSRV;
	ID3D11SamplerState* shadowSampler;
	ID3D11RasterizerState* shadowRasterizer;
	SimpleVertexShader* shadowVS;
	DirectX::XMFLOAT4X4 shadowViewMatrix;
	DirectX::XMFLOAT4X4 shadowProjectionMatrix;
	DirectionalLight* light;

public:
	ShadowMap();
	ShadowMap(int size, DirectionalLight* light);
	~ShadowMap();
	void Init(ID3D11Device* device, ID3D11DeviceContext* context);

	SimpleVertexShader* GetShadowVS();
	void PrepareContext(ID3D11DeviceContext* context);


	DirectX::XMFLOAT4X4 GetViewMatrix();
	DirectX::XMFLOAT4X4 GetProjectionMatrix();

	ID3D11SamplerState* GetSampler();
	ID3D11ShaderResourceView* GetSRV();

private:
	void RecalculateMatrices();
};