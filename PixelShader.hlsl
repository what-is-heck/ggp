// Struct representing the data we expect to receive from earlier pipeline stages
// - Should match the output of our corresponding vertex shader
// - The name of the struct itself is unimportant
// - The variable names don't have to match other shaders (just the semantics)
// - Each variable must have a semantic, which defines its usage
struct VertexToPixel
{
	// Data type
	//  |
	//  |   Name          Semantic
	//  |    |             |
	//  v    v             v
	float4 position : SV_POSITION;	// XYZW position (System Value Position)
	float4 vpos     : VPOS;
	float3 normal   : NORMAL;
	float3 tangent  : TANGENT;
	float2 uv       : TEXCOORD;
	float4 posForShadow1 : POSITION1;
	float4 posForShadow2 : POSITION2;
};

struct DirectionalLight
{
	float4 Ambient;
	float4 Diffuse;
	float3 Direction;
};

cbuffer LightData : register(b0)
{
	DirectionalLight light;
	DirectionalLight light2;

	float3 camPos; // World-space camera position
};

SamplerState ss      : register(s0);
SamplerComparisonState ShadowSampler : register(s1);

Texture2D    diffuse : register(t0);
Texture2D    spec    : register(t1);
Texture2D    normal  : register(t2);
Texture2D ShadowMap1 : register(t3);
Texture2D ShadowMap2 : register(t4);

// --------------------------------------------------------
// The entry point (main method) for our pixel shader
// 
// - Input is the data coming down the pipeline (defined by the struct)
// - Output is a single color (float4)
// - Has a special semantic (SV_TARGET), which means 
//    "put the output of this into the current render target"
// - Named "main" because that's the default the shader compiler looks for
// --------------------------------------------------------
float4 main(VertexToPixel input) : SV_TARGET
{
	float4 surfaceCol = diffuse.Sample(ss, input.uv);
	float4 surfaceSpc = spec.Sample(ss, input.uv);
	float4 surfaceNor = normal.Sample(ss, input.uv) * 2.0 - 1.0;

	// Normal mapping
	float3 nor = normalize(input.normal);
	float3 tan = normalize(input.tangent);
	float3 bit = cross(nor, tan);

	float3x3 tbn = float3x3(tan, bit, nor);

	// Final normal after mapping
	float3 norm = normalize(mul((float3)surfaceNor, tbn));

	float3 dirToLight = normalize(-light.Direction);
	float3 ndl = saturate(dot(dirToLight, norm));

	float3 dtl2 = normalize(-light2.Direction);
	float3 ndl2 = saturate(dot(dtl2, norm));

	// Blinn-Phong specular

	float3 dtc = normalize(camPos - input.vpos);

	float3 hvec1 = normalize(dirToLight + dtc);
	float3 hvec2 = normalize(dtl2 + dtc);
	float  ndh1  = saturate(dot(norm, hvec1));
	float  ndh2  = saturate(dot(norm, hvec2));
	float  spec1 = pow(ndh1, 64);
	float  spec2 = pow(ndh2, 64);


	// Shadow mapping stuff =============

	// Convert from NDC to UV coords (flipping the Y)
	float2 shadowUV1 = input.posForShadow1.xy / input.posForShadow1.w * 0.5f + 0.5f;
	shadowUV1.y = 1.0f - shadowUV1.y;

	// The actual depth from the light to the surface
	float depthFromLight1 = input.posForShadow1.z / input.posForShadow1.w;

	// Sample the shadow map, using the hardware to automatically
	// do a comparison for us (which will compare several pixels)
	float shadowAmount1 = ShadowMap1.SampleCmpLevelZero(
		ShadowSampler,
		shadowUV1,
		depthFromLight1);

	// Convert from NDC to UV coords (flipping the Y)
	float2 shadowUV2 = input.posForShadow2.xy / input.posForShadow2.w * 0.5f + 0.5f;
	shadowUV2.y = 1.0f - shadowUV2.y;

	// The actual depth from the light to the surface
	float depthFromLight2 = input.posForShadow2.z / input.posForShadow2.w;

	// Sample the shadow map, using the hardware to automatically
	// do a comparison for us (which will compare several pixels)
	float shadowAmount2 = ShadowMap2.SampleCmpLevelZero(
		ShadowSampler,
		shadowUV2,
		depthFromLight2);

	return
								   
		(((float4(ndl, 1.0)  * light.Diffuse * shadowAmount1 + light.Ambient) +
			float4(ndl2, 1.0) * light2.Diffuse * shadowAmount2 + light2.Ambient) * surfaceCol +
			// Reflecting the actual light's color just seems to look nicer
			surfaceSpc * 4 * spec1 * light.Diffuse * shadowAmount1 + surfaceSpc * 4 * spec2 * light2.Diffuse * shadowAmount2);
				
				
}