#ifndef CAMERAENTITY_H_
#define CAMERAENTITY_H_

#include "Entity.h"

class CameraEntity : public Entity
{
private:
	// Cached matrices saves from excessive recalculation
	DirectX::XMFLOAT4X4 viewMatrix;
	DirectX::XMFLOAT4X4 projMatrix;

	// Vertical field of view, in radians
	float fov;

	// Near and far clip planes, in arbitrary space distance units
	float ncp;
	float fcp;

	// Whether the view matrix needs to be recalculated
	bool needsViewUpdated;
public:
	CameraEntity();
	CameraEntity(Game* g, const Transform& t = Transform(), Entity* parentEntity = nullptr, float fovRads = 1.0f, float nearClip = 0.1f, float farClip = 100.0f);
	virtual ~CameraEntity();

	inline DirectX::XMFLOAT4X4 GetProjMatrix() const { return projMatrix; }

	inline float GetFOV() { return fov; }
	inline float GetNCP() { return ncp; }
	inline float GetFCP() { return fcp; }
	inline void SetFOV(float fovRads) { fov = fovRads; }
	inline void SetNCP(float nearClip) { ncp = nearClip; }
	inline void SetFCP(float farClip) { fcp = farClip; }

	DirectX::XMFLOAT4X4 GetViewMatrix();

	void OnResize(float w, float h);
};

#endif