#include "CameraEntity.h"

#include <Windows.h>

CameraEntity::CameraEntity() :
	viewMatrix(),
	projMatrix(),
	fov(1.0f),
	ncp(0.1f),
	fcp(100.0f),
	needsViewUpdated(true),
	Entity()
{
	// Nothing interesting to do here
}

CameraEntity::CameraEntity(Game* g, const Transform& t, Entity* parentEntity, float fovRads, float nearClip, float farClip) :
	viewMatrix(),
	projMatrix(),
	fov(fovRads),
	ncp(nearClip),
	fcp(farClip),
	needsViewUpdated(true),
	Entity(g, t, parentEntity)
{
	// Nothing interesting to do here
}

CameraEntity::~CameraEntity()
{
	// Nothing interesting to do here
}

DirectX::XMFLOAT4X4 CameraEntity::GetViewMatrix()
{
	if (false)
	{
		Transform w = GetWorldTransform();
		DirectX::XMMATRIX view = DirectX::XMMatrixLookToLH(
			w.GetPosition(),
			w.ForwardVector(),
			w.UpVector());

		DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(view));

		needsViewUpdated = false;
	}

	Transform w = GetWorldTransform();
	DirectX::XMMATRIX view = DirectX::XMMatrixLookToLH(
		w.GetPosition(),
		w.ForwardVector(),
		w.UpVector());

	DirectX::XMStoreFloat4x4(&viewMatrix, XMMatrixTranspose(view));

	return viewMatrix;
}

void CameraEntity::OnResize(float w, float h)
{
	DirectX::XMMATRIX P = DirectX::XMMatrixPerspectiveFovLH(
		fov,      // Field of View Angle
		w / h,    // Aspect ratio
		ncp,      // Near clip plane distance
		fcp);     // Far clip plane distance
	XMStoreFloat4x4(&projMatrix, DirectX::XMMatrixTranspose(P)); // Transpose for HLSL!
}