#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <vector>
#include <memory>

class SimpleVertexShader;
class SimplePixelShader;
class SimpleTexture;

struct ID3D11ShaderResourceView;
struct ID3D11SamplerState;

// TODO : Account for different shader types and combinations

class Material
{
private:
	// Shaders
	SimpleVertexShader* vertexShader;
	SimplePixelShader* pixelShader;

	// Textures
	std::vector<std::string> texNames;
	std::vector<std::shared_ptr<SimpleTexture>> textures;
	ID3D11SamplerState* sampler;
public:
	Material();
	Material(SimpleVertexShader* v, SimplePixelShader* p, std::vector<std::string> tn, std::vector<std::shared_ptr<SimpleTexture>> tx, ID3D11SamplerState* ss);
	~Material();

	inline SimpleVertexShader* GetVertexShader() { return vertexShader; }
	inline SimplePixelShader*  GetPixelShader()  { return pixelShader; }

	inline std::vector<std::string>& GetTexNames() { return texNames; }
	inline std::vector<std::shared_ptr<SimpleTexture>>& GetTextures() { return textures; }

	inline ID3D11SamplerState* GetSampler() { return sampler; }
};

#endif