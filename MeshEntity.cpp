#include "MeshEntity.h"

MeshEntity::MeshEntity() :
	mesh(nullptr),
	material(nullptr),
	Entity()
{
	// Nothing interesting to do here
}

MeshEntity::MeshEntity(Game* g, std::shared_ptr<Mesh> m, std::shared_ptr<Material> mat, const Transform& t, Entity* parentEntity) :
	mesh(m),
	material(mat),
	Entity(g, t, parentEntity)
{
	// Nothing interesting to do here
}

MeshEntity::~MeshEntity()
{
	// Nothing interesting to do here
}