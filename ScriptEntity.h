#ifndef SCRIPTENTITY_H_
#define SCRIPTENTITY_H_

#include "Entity.h"

class Script;

class ScriptEntity : public Entity
{
private:
	// Script attached to this, the entity owns the script and will deal with cleanup
	Script* script;
public:
	// Don't use this
	ScriptEntity();
	// A script must be allocated for this call, after which this will become its owner and take care of cleanup
	ScriptEntity(Game* g, Script* s = nullptr, const Transform& t = Transform(), Entity* parentEntity = nullptr);
	virtual ~ScriptEntity();

	inline Script* GetScript() { return script; }
	void SetScript(Script* s);

	// Event hook for the update loop, which will call into this entity's script
	void Update(float dt);
};

#endif