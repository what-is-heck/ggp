#include "Transform.h"

Transform::Transform() :
	pos(DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f)),
	rot(DirectX::XMQuaternionIdentity()),
	sca(DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f))
{
	// Nothing interesting to do here
}

Transform::Transform(const Transform& copy) :
	pos(copy.pos),
	rot(copy.rot),
	sca(copy.sca)
{
	// Nothing interesting to do here
}

Transform::Transform(DirectX::XMVECTOR position,
                     DirectX::XMVECTOR rotation,
                     DirectX::XMVECTOR scale) :
	pos(position),
	rot(rotation),
	sca(scale)
{
	// Nothing interesting to do here
}

Transform::~Transform()
{
	// Nothing interesting to do here
}

Transform& Transform::operator= (const Transform& copy)
{
	pos = copy.pos;
	rot = copy.rot;
	sca = copy.sca;

	return *this;
}

Transform operator* (const Transform& a, const Transform& b)
{
	Transform res(a);

	res.TranslateLocal(b.pos);
	res.RotateLocal(b.rot);

	// Scale is to remain unaffected; this creates less problems down the road
	res.sca = b.sca;

	return res;
}

DirectX::XMVECTOR Transform::ForwardVector() const
{
	DirectX::XMVECTOR vec = DirectX::XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	return DirectX::XMVector3Rotate(vec, rot);
}

DirectX::XMVECTOR Transform::RightVector() const
{
	DirectX::XMVECTOR vec = DirectX::XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);
	return DirectX::XMVector3Rotate(vec, rot);
}

DirectX::XMVECTOR Transform::UpVector() const
{
	DirectX::XMVECTOR vec = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	return DirectX::XMVector3Rotate(vec, rot);
}

void Transform::TranslateLocal(DirectX::XMVECTOR v)
{
	pos = DirectX::XMVectorAdd(pos, DirectX::XMVector3Rotate(v, rot));
}

void Transform::TranslateGlobal(DirectX::XMVECTOR v)
{
	pos = DirectX::XMVectorAdd(pos, v);
}

void Transform::RotateLocal(DirectX::XMVECTOR q)
{
	rot = DirectX::XMQuaternionMultiply(q, rot);
}

void Transform::RotateGlobal(DirectX::XMVECTOR q)
{
	rot = DirectX::XMQuaternionMultiply(rot, q);
}

void Transform::ScaleAdd(DirectX::XMVECTOR v)
{
	sca = DirectX::XMVectorAdd(sca, v);
}

void Transform::ScaleMul(DirectX::XMVECTOR v)
{
	sca = DirectX::XMVectorMultiply(sca, v);
}

DirectX::XMFLOAT4X4 Transform::GetMatrix() const
{
	DirectX::XMMATRIX temp =
	DirectX::XMMatrixAffineTransformation(sca,
	                                      DirectX::XMVectorSet(0, 0, 0, 0),
	                                      rot,
	                                      pos);
	DirectX::XMFLOAT4X4 res;
	DirectX::XMStoreFloat4x4(&res, temp);

	return res;
}