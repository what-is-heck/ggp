#pragma once
#include "SimpleShader.h"

class Skybox
{
private:
	// Render states
	ID3D11RasterizerState* skyRastState;
	ID3D11DepthStencilState* skyDepthState;
	bool initld = false;
public:
	Skybox();
	~Skybox();

	void init(ID3D11Device* device);

	ID3D11DepthStencilState* getStencilState();
	ID3D11RasterizerState* getRastState();

};