#include "Game.h"
#include "Vertex.h"
#include "Entity.h"
#include "CameraEntity.h"
#include "MeshEntity.h"
#include "ScriptEntity.h"
#include "Scripts/CameraScript.h"
#include "Scripts/RotatorScript.h"
#include "SimpleTexture.h"

#include <WICTextureLoader.h>

#include <cmath>

// For the DirectX Math library
using namespace DirectX;
// --------------------------------------------------------
// Constructor
//
// DXCore (base class) constructor will set up underlying fields.
// DirectX itself, and our window, are not ready yet!
//
// hInstance - the application's OS-level handle (unique ID)
// --------------------------------------------------------
Game::Game(HINSTANCE hInstance)
	: DXCore( 
		hInstance,		   // The application's handle
		"DirectX Game",	   // Text for the window's title bar
		1280,			   // Width of the window's client area
		720,			   // Height of the window's client area
		true)			   // Show extra stats (fps) in title bar?
{
	// Initialize fields
	time = 0.0f;

	primaryCamera = nullptr;

#if defined(DEBUG) || defined(_DEBUG)
	// Do we want a console window?  Probably only in debug mode
	CreateConsoleWindow(500, 120, 32, 120);
	printf("Console window created successfully.  Feel free to printf() here.\n");
#endif
}

// --------------------------------------------------------
// Destructor - Clean up anything our game has created:
//  - Release all DirectX objects created here
//  - Delete any objects to prevent memory leaks
// --------------------------------------------------------
Game::~Game()
{
	//Entities are destroyed by the scene that spawned them. We just take care of the materials here.
	materials.Destroy();
}

// --------------------------------------------------------
// Called once per program, after DirectX and the window
// are initialized but before the game loop.
// --------------------------------------------------------
void Game::Init()
{
	// Tell the input assembler stage of the pipeline what kind of
	// geometric primitives (points, lines or triangles) we want to draw.  
	// Essentially: "What kind of shape should the GPU draw with our data?"
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	//initialize our caches
	meshes = MeshCache(device);
	materials = MaterialCache(device, context);
	materials.Init();

	//set up the renderer
	renderer = Renderer();
	renderer.Init(device, context, swapChain, backBufferRTV, depthStencilView);
	renderer.OnResize(width, height);

	//load in a scene
	s = Scene(this, "test.sc");
	s.Init(&meshes, &materials);

	renderer.UpdateRenderables(s.GetAll<MeshEntity>());
	renderer.UpdateParticleSystems(s.GetAll<ParticleEntity>());

	//Set the primary camera
	CameraEntity* main = (CameraEntity*)(s.MatchesTag("MainCamera").at(0));
	renderer.SetCamera(main, "MainCamera");
	SetPrimaryCamera(main);
}

// --------------------------------------------------------
// Handle resizing DirectX "stuff" to match the new window size.
// For instance, updating our projection matrix's aspect ratio.
// --------------------------------------------------------
void Game::OnResize()
{
	// Handle base-level DX resize stuff
	DXCore::OnResize();

	// Update our projection matrix since the window size changed
	if (primaryCamera) { primaryCamera->OnResize(width, height); }

	renderer.OnResize(width, height);
}

// --------------------------------------------------------
// Update your game here - user input, move objects, AI, etc.
// --------------------------------------------------------
void Game::Update(float deltaTime, float totalTime)
{
	// Quit if the escape key is pressed
	if (GetAsyncKeyState(VK_ESCAPE))
		Quit();

	s.Update(deltaTime, totalTime);

	time += deltaTime;

	mstate.dx = 0;
	mstate.dy = 0;
	mstate.dw = 0.0f;
}

// --------------------------------------------------------
// Clear the screen, redraw everything, present to the user
// --------------------------------------------------------
void Game::Draw(float deltaTime, float totalTime)
{
	// Background color (Cornflower Blue in this case) for clearing
	const float color[4] = {0.3f, 0.4f, 0.5f, 0.0f};

	// Clear the render target and depth buffer (erases what's on the screen)
	//  - Do this ONCE PER FRAME
	//  - At the beginning of Draw (before drawing *anything*)
	context->ClearRenderTargetView(backBufferRTV, color);
	context->ClearDepthStencilView(
		depthStencilView, 
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);

	renderer.Draw(deltaTime, totalTime);
}

void Game::SetPrimaryCamera(CameraEntity* c)
{
	primaryCamera = c;
	if (c)
	{
		c->OnResize(width, height);
	}
}

#pragma region Mouse Input

// --------------------------------------------------------
// Helper method for mouse clicking.  We get this information
// from the OS-level messages anyway, so these helpers have
// been created to provide basic mouse input if you want it.
// --------------------------------------------------------
void Game::OnMouseDown(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...
	mstate.lmb = true;

	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;

	// Caputure the mouse so we keep getting mouse move
	// events even if the mouse leaves the window.  we'll be
	// releasing the capture once a mouse button is released
	SetCapture(hWnd);
}

// --------------------------------------------------------
// Helper method for mouse release
// --------------------------------------------------------
void Game::OnMouseUp(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...
	mstate.lmb = false;

	// We don't care about the tracking the cursor outside
	// the window anymore (we're not dragging if the mouse is up)
	ReleaseCapture();
}

// --------------------------------------------------------
// Helper method for mouse movement.  We only get this message
// if the mouse is currently over the window, or if we're 
// currently capturing the mouse.
// --------------------------------------------------------
void Game::OnMouseMove(WPARAM buttonState, int x, int y)
{
	// Add any custom code here...
	if (!(GetAsyncKeyState('M') & 0x8000) && time > 0.1f)
	{
		mstate.dx += x - mstate.x;
		mstate.dy += y - mstate.y;
	}
	mstate.x = x;
	mstate.y = y;

	// if (!(GetAsyncKeyState('M') & 0x8000) && time > 0.1f) { camera.HandleMouse(x - prevMousePos.x, y - prevMousePos.y); }

	// Save the previous mouse position, so we have it for the future
	prevMousePos.x = x;
	prevMousePos.y = y;

	// Good job, me. I'm leaving this here just so you have to look at it every time you're here again.
	// camera.HandleMouse(x - prevMousePos.x, y - prevMousePos.y);
}

// --------------------------------------------------------
// Helper method for mouse wheel scrolling.  
// WheelDelta may be positive or negative, depending 
// on the direction of the scroll
// --------------------------------------------------------
void Game::OnMouseWheel(float wheelDelta, int x, int y)
{
	// Add any custom code here...
	mstate.dw += wheelDelta;
}
#pragma endregion