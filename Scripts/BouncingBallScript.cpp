#include "BouncingBallScript.h"
#include "../Physics.h"
#include "../PhysicsEntity.h"

#include <cstdio>

BouncingBallScript::BouncingBallScript() :
	PhysicsScript()
{
	printf("BOUNCING BALL CREATED : %p\n", nullptr);
}

BouncingBallScript::BouncingBallScript(PhysicsEntity* b) :
	PhysicsScript(b)
{
	printf("BOUNCING BALL CREATED : %p\n", Body());
}

BouncingBallScript::~BouncingBallScript()
{
	// Nothing interesting to do here
}

void BouncingBallScript::Update(float dt)
{
	// Nothing interesting to do here
}

void BouncingBallScript::CollisionEnter(PhysicsEntity* other)
{
	printf("BOUNCING BALL HIT ENTITY : %p\n", other);
}

void BouncingBallScript::CollisionExit(PhysicsEntity* other)
{
	printf("BOUNCING BALL UNHIT ENTITY : %p\n", other);
}