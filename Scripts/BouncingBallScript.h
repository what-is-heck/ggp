#ifndef BOUNCINGBALLSCRIPT_H_
#define BOUNCINGBALLSCRIPT_H_

#include "PhysicsScript.h"

class BouncingBallScript : public PhysicsScript
{
public:
	BouncingBallScript();
	BouncingBallScript(PhysicsEntity* b);
	virtual ~BouncingBallScript();

	// Event hook for the update loop
	virtual void Update(float dt);

	virtual void CollisionEnter(PhysicsEntity* other);
	virtual void CollisionExit(PhysicsEntity* other);
};

#endif