#include "RotatorScript.h"

#include <Windows.h>
#include <cstdio>

RotatorScript::RotatorScript() :
	Script(),
	spd(1.0f)
{
	// Nothing interesting to do here
}

RotatorScript::RotatorScript(float speed) :
	Script(),
	spd(speed)
{
	// Nothing interesting to do here
}

RotatorScript::~RotatorScript()
{
	// Nothing interesting to do here
}

void RotatorScript::Update(float dt)
{
	Self()->Rotate(DirectX::XMQuaternionRotationRollPitchYaw(0.0f, spd * dt, 0.0f));
}