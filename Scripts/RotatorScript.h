#ifndef ROTATORSCRIPT_H_
#define ROTATORSCRIPT_H_

#include "../Script.h"

class RotatorScript : public Script
{
private:
	// Rotation speed (rad / sec)
	float spd;
public:
	RotatorScript();
	RotatorScript(float speed);
	virtual ~RotatorScript();

	// Event hook for the update loop
	virtual void Update(float dt);
};

#endif