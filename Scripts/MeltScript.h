#ifndef MELTSCRIPT_H_
#define MELTSCRIPT_H_

#include "../Script.h"

#include <memory>

class Material;
class GoalEmitter;

class MeltScript : public Script
{
private:
	// ref to the material we'll be swapping to
	bool swapped = false;
	float spd = 0;
	//time
	float time;

	GoalEmitter* goal1;
	GoalEmitter* goal2;
public:
	MeltScript();
	MeltScript(GoalEmitter* g1, GoalEmitter* g2);
	virtual ~MeltScript();
	void Melt(); 
	float getTime();
	bool isActive();

	// Event hook for the update loop
	virtual void Update(float dt);
};

#endif