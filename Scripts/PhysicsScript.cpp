#include "PhysicsScript.h"
#include "../Physics.h"
#include "../PhysicsEntity.h"

#include <cstdio>

PhysicsScript::PhysicsScript() :
	Script(),
	body(nullptr)
{
	// Nothing interesting to do here
}

PhysicsScript::PhysicsScript(PhysicsEntity* b) :
	Script(),
	body(b)
{
	b->RegisterEnterListener(this);
	b->RegisterExitListener(this);
}

PhysicsScript::~PhysicsScript()
{
	if (body)
	{
		body->UnregisterEnterListener(this);
		body->UnregisterExitListener(this);
	}
}

void PhysicsScript::Update(float dt)
{
	// Nothing interesting to do here
}

void PhysicsScript::CollisionEnter(PhysicsEntity* other)
{
	// Nothing interesting to do here
}

void PhysicsScript::CollisionExit(PhysicsEntity* other)
{
	// Nothing interesting to do here
}