#include "CameraScript.h"

#include <Windows.h>
#include <cstdio>

#include "../PhysicsEntity.h"

CameraScript::CameraScript() :
	ball(nullptr),
	prev(false),
	Script()
{
	// Nothing interesting to do here
}

CameraScript::CameraScript(PhysicsEntity* b) :
	ball(b),
	prev(false),
	Script()
{
	// Nothing interesting to do here
}

CameraScript::~CameraScript()
{
	// Nothing interesting to do here
}

void CameraScript::Update(float dt)
{
	Self()->SetPosition(ball->GetWorldTransform().GetPosition());

	if ((GetAsyncKeyState(VK_SPACE) & 0x8000) && !prev)
	{
		prev = true;

		DirectX::XMVECTOR fwd = Self()->GetWorldTransform().ForwardVector();
		float x = DirectX::XMVectorGetX(fwd) * 20.0f;
		float y = DirectX::XMVectorGetY(fwd) * 20.0f;
		float z = DirectX::XMVectorGetZ(fwd) * 20.0f;

		ball->RigidBody()->applyImpulse(btVector3(x, y, z), btVector3(0.0f, 0.0f, 0.0f));
	}
	else if (!(GetAsyncKeyState(VK_SPACE) & 0x8000))
	{
		prev = false;
	}

	Self()->RotateWorld(DirectX::XMQuaternionRotationRollPitchYaw(0.0f, GetMouseState()->dx * 0.008f, 0.0f));
	Self()->RotateLocal(DirectX::XMQuaternionRotationRollPitchYaw(GetMouseState()->dy * 0.008f, 0.0f, 0.0f));
}