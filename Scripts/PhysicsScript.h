#ifndef PHYSICSSCRIPT_H_
#define PHYSICSSCRIPT_H_

#include "../Script.h"

class Entity;
class PhysicsEntity;

class PhysicsScript : public Script
{
private:
	PhysicsEntity* body;
protected:
	inline PhysicsEntity* Body() { return body; }
public:
	PhysicsScript();
	PhysicsScript(PhysicsEntity* b);
	virtual ~PhysicsScript();

	// Event hook for the update loop
	virtual void Update(float dt);

	virtual void CollisionEnter(PhysicsEntity* other);
	virtual void CollisionExit(PhysicsEntity* other);
};

#endif