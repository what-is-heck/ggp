#include <Windows.h>
#include <cstdio>
#include "MeltScript.h"
#include "../MeshEntity.h"
#include "../PhysicsEntity.h"
#include "../GoalEmitter.h"

MeltScript::MeltScript() :
	Script(),
	spd(1.0f)
{
	// Nothing interesting to do here
}

MeltScript::MeltScript(GoalEmitter* g1, GoalEmitter* g2) :
	goal1(g1),
	goal2(g2),
	Script()
{
	// Nothing interesting to do here
}

MeltScript::~MeltScript()
{
	// Nothing interesting to do here
}

void MeltScript::Update(float dt)
{
	if (!swapped && Self()->GetWorldTransform().GetMatrix()._42 < -6)//Y
	{
		Melt();

		if (DirectX::XMVectorGetZ(Self()->GetWorldTransform().GetPosition()) < 0.0f)
		{
			goal1->Goal();
		}
		else
		{
			goal2->Goal();
		}
	}
	else if(swapped){
		time += dt;
	}
}

void MeltScript::Melt()
{
	MeshEntity* p =
		((MeshEntity*)(Self()->GetParent()));
	PhysicsEntity* p2 = (PhysicsEntity*)p->GetParent();

	p2->RigidBody()->setGravity(btVector3(0, 0, 0));
	p2->RigidBody()->applyImpulse(btVector3(0, 5, 0), btVector3(0, 0, 0));
	swapped = true;
}

float MeltScript::getTime()
{
	return time;
}

bool MeltScript::isActive()
{
	return swapped;
}
