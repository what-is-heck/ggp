#ifndef CAMERASCRIPT_H_
#define CAMERASCRIPT_H_

#include "../Script.h"

class PhysicsEntity;

class CameraScript : public Script
{
private:
	PhysicsEntity* ball;
	bool prev;
public:
	CameraScript();
	CameraScript(PhysicsEntity* b);
	virtual ~CameraScript();

	// Event hook for the update loop
	virtual void Update(float dt);
};

#endif