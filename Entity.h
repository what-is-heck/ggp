#ifndef ENTITY_H_
#define ENTITY_H_

#include <DirectXMath.h>
#include <vector>
#include "Transform.h"

class Game;

// TODO : Add useful features like a name / tag / ID system
class Entity
{
private:
	std::vector<Entity*> children;
	Entity* parent;

	// Cached world matrix saves from excessive recalculation
	DirectX::XMFLOAT4X4 worldMatrix;

	Transform local;
	// Cached world transform saves from excess parent tree traversal and recalculation
	Transform world;

	// Control variables for determining when to recalculate cached variables
	bool needsTransformUpdated;
	bool needsMatrixUpdated;

	// Helper methods
	void AddChild(Entity* newChild);
	void RemoveChild(Entity* child);
	void RequireUpdate();
protected:
	// Hold on to a pointer to the Game, just in case I decide to use it for other things later
	Game* game;
public:
	Entity();
	Entity(Game* g, const Transform& t = Transform(), Entity* parentEntity = nullptr);
	virtual ~Entity();

	inline Entity* GetParent() { return parent; }
	inline std::vector<Entity*> GetChildren() { return children; }
	inline size_t GetChildCount() { return children.size(); }

	inline Transform GetTransform() const { return local; }

	Transform GetWorldTransform();
	DirectX::XMFLOAT4X4 GetWorldMatrix();

	// Changes the parent Entity to another or none
	void SetParent(Entity* newParent, bool keepWorldTransform);

	// Set position with respect to parent
	void __vectorcall SetPosition(DirectX::XMVECTOR position);
	// Set position with respect to self
	void __vectorcall SetPositionLocal(DirectX::XMVECTOR position);
	// Set position with respect to world
	void __vectorcall SetPositionWorld(DirectX::XMVECTOR position);
	// Set rotation with respect to parent
	void __vectorcall SetRotation(DirectX::XMVECTOR rotation);
	// Set rotation with respect to world
	void __vectorcall SetRotationWorld(DirectX::XMVECTOR rotation);
	// Set scale
	void __vectorcall SetScale(DirectX::XMVECTOR scale);

	// Translate with respect to parent
	void __vectorcall Translate(DirectX::XMVECTOR v);
	// Translate with respect to self
	void __vectorcall TranslateLocal(DirectX::XMVECTOR v);
	// Translate with respect to world
	void __vectorcall TranslateWorld(DirectX::XMVECTOR v);
	// Rotate with respect to parent
	void __vectorcall Rotate(DirectX::XMVECTOR q);
	// Rotate with respect to self
	void __vectorcall RotateLocal(DirectX::XMVECTOR q);
	// Rotate with respect to world
	void __vectorcall RotateWorld(DirectX::XMVECTOR q);
	// Scale
	void __vectorcall Scale(DirectX::XMVECTOR v);
};

#endif