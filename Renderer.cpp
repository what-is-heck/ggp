#include "Renderer.h"
#include "Vertex.h"
#include "SimpleTexture.h"

#include "Emitter.h"
#include "Scripts\MeltScript.h"
#include "ParticleEntity.h"
#include "DDSTextureLoader.h"

Renderer::Renderer()
{
	primaryCamera = nullptr;
	// \(o_>o)/ REFACTOR or RIOT \(o_>o)/
	// light = { { +0.08f, +0.08f, +0.07f, +1.00f },
	light = { { +0.20f, +0.20f, +0.20f, +1.00f },
	{ +0.50f, +0.45f, +0.40f, +1.00f },
	{ +0.10f, -1.00f, -0.60f } };

	// light2 = { { +0.07f, +0.06f, +0.05f, +1.00f },
	light2 = { { +0.30f, +0.30f, +0.35f, +1.00f },
	{ +0.10f, +0.10f, +0.10f, +1.00f },
	{ +0.00f, -1.00f, +0.01f } };

	particleVS = nullptr;
	particlePS = nullptr;
	particleIB = nullptr;
}


Renderer::~Renderer()
{
	if (particleIB)
	{
		delete particleVS;
		delete particlePS;
		particleIB->Release();
		particleBS->Release();
		particleDS->Release();
	}
}

void Renderer::Init(ID3D11Device* device,
	ID3D11DeviceContext * context,
	IDXGISwapChain * swapChain,
	ID3D11RenderTargetView* backBufferRTV,
	ID3D11DepthStencilView* depthStencilView)
{
	//store references
	this->device = device;
	this->swapChain = swapChain;
	this->context = context;

	this->depthStencilView = depthStencilView;
	this->backBufferRTV = backBufferRTV;

	//create anything
	// Since we're super CPU bound anyway, why not crank these up, eh?
	shadowMap1 = ShadowMap(8192, &light);//4096
	shadowMap1.Init(device, context);
	shadowMap2 = ShadowMap(8192, &light2);
	shadowMap2.Init(device, context);

	// Particles

	// Setup the index buffer
	unsigned int* indices = new unsigned int[MAX_PARTICLES * 6];
	int indexCount = 0;
	for (int i = 0; i < MAX_PARTICLES * 4; i += 4)
	{
		indices[indexCount++] = i;
		indices[indexCount++] = i + 1;
		indices[indexCount++] = i + 2;
		indices[indexCount++] = i;
		indices[indexCount++] = i + 2;
		indices[indexCount++] = i + 3;
	}
	D3D11_SUBRESOURCE_DATA indexData = {};
	indexData.pSysMem = indices;

	// Regular index buffer
	D3D11_BUFFER_DESC ibDesc = {};
	ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibDesc.CPUAccessFlags = 0;
	ibDesc.Usage = D3D11_USAGE_DEFAULT;
	ibDesc.ByteWidth = sizeof(unsigned int) * MAX_PARTICLES * 6;
	device->CreateBuffer(&ibDesc, &indexData, &particleIB);

	delete[] indices;
	// This is okay because it's used exclusively by Renderer
	particleVS = new SimpleVertexShader(device, context);
	if (!(particleVS->LoadShaderFile(std::wstring(L"x64/Debug/ParticleVS.cso").c_str())))
	{
		particleVS->LoadShaderFile(std::wstring(L"ParticleVS.cso").c_str());
	}
	particlePS = new SimplePixelShader(device, context);
	if (!(particlePS->LoadShaderFile(std::wstring(L"x64/Debug/ParticlePS.cso").c_str())))
	{
		particlePS->LoadShaderFile(std::wstring(L"ParticlePS.cso").c_str());
	}

	// A depth state for the particles
	D3D11_DEPTH_STENCIL_DESC dsDesc = {};
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO; // Turns off depth writing
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;
	device->CreateDepthStencilState(&dsDesc, &particleDS);


	// Blend for particles (additive)
	D3D11_BLEND_DESC blend = {};
	blend.AlphaToCoverageEnable = false;
	blend.IndependentBlendEnable = false;
	blend.RenderTarget[0].BlendEnable = true;
	blend.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blend.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
	blend.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blend.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blend.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blend.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blend.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	device->CreateBlendState(&blend, &particleBS);

	//skybox
	//skybox = Skybox();
	skybox.init(device);
}

void Renderer::SetCamera(CameraEntity * camera, std::string tag)
{
	this->primaryCamera = camera;
	//tag: currently unsupported (always sets primary)
}

void Renderer::UpdateRenderables(std::vector<MeshEntity*> renderables)
{
	this->renderables = renderables;
}

void Renderer::UpdateParticleSystems(std::vector<ParticleEntity*> p)
{
	emitters = p;
}

void Renderer::Draw(float deltaTime, float totalTime) {
	// Background color (Cornflower Blue in this case) for clearing
	const float color[4] = { 0.3f, 0.4f, 0.5f, 0.0f };

	// If there is no camera set, don't bother rendering anything to the screen
	if (primaryCamera == nullptr)
	{
		swapChain->Present(1, 0);
		return;
	}
	//Update the shadow map
	//if(totalTime>1.0f)
	RenderShadowMaps();


	// Clear the render target and depth buffer (erases what's on the screen)
	//  - Do this ONCE PER FRAME
	//  - At the beginning of Draw (before drawing *anything*)
	context->ClearRenderTargetView(backBufferRTV, color);
	context->ClearDepthStencilView(
		depthStencilView,
		D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL,
		1.0f,
		0);

	// Send data to shader variables
	//  - Do this ONCE PER OBJECT you're drawing
	//  - This is actually a complex process of copying data to a local buffer
	//    and then copying that entire buffer to the GPU.  
	//  - The "SimpleShader" class handles all of that for you.

	// vertexShader->SetMatrix4x4("view", primaryCamera->GetViewMatrix());
	// vertexShader->SetMatrix4x4("projection", primaryCamera->GetProjMatrix());

	// Once you've set all of the data you care to change for
	// the next draw call, you need to actually send it to the GPU
	//  - If you skip this, the "SetMatrix" calls above won't make it to the GPU!

	// vertexShader->CopyAllBufferData();

	// Set the vertex and pixel shaders to use for the next Draw() command
	//  - These don't technically need to be set every frame...YET
	//  - Once you start applying different shaders to different objects,
	//    you'll need to swap the current shaders before each draw

	// vertexShader->SetShader();
	// pixelShader->SetShader();

	// Set buffers in the input assembler
	//  - Do this ONCE PER OBJECT you're drawing, since each object might
	//    have different geometry.
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	// Finally do the actual drawing
	//  - Do this ONCE PER OBJECT you intend to draw
	//  - This will use all of the currently set DirectX "stuff" (shaders, buffers, etc)
	//  - DrawIndexed() uses the currently set INDEX BUFFER to look up corresponding
	//     vertices in the currently set VERTEX BUFFER

	// context->DrawIndexed(
	//	3,     // The number of indices to use (we could draw a subset if we wanted)
	//	0,     // Offset to the first index we want to use
	//	0);    // Offset to add to each index when looking up vertices

	DirectX::XMFLOAT3 camPos;
	DirectX::XMStoreFloat3(&camPos, primaryCamera->GetWorldTransform().GetPosition());

	size_t ec = renderables.size();

	//sigh
	//ID3D11ShaderResourceView* sky = renderables[ec - 1]->GetMaterial()->GetTextures()[0]->GetSRV();
	//ps->SetShaderResourceView(texNames[i], (*(textures[i])).GetSRV());

	//caching!
	SimpleVertexShader* vs = 0;
	SimplePixelShader* ps = 0;
	for (size_t i = 0; i < ec; ++i)
	{

		//someday when we have tags at the entity level, this will be a lot cleaner
		//check for melt timer
		// Swap the material, if need be
		if (i == 0 || renderables[i]->GetMaterial() != renderables[i - 1]->GetMaterial())
		{
			vs = renderables[i]->GetMaterial()->GetVertexShader();
			ps = renderables[i]->GetMaterial()->GetPixelShader();

			vs->SetMatrix4x4("view", primaryCamera->GetViewMatrix());
			vs->SetMatrix4x4("projection", primaryCamera->GetProjMatrix());
			vs->SetMatrix4x4("shadowView1", shadowMap1.GetViewMatrix());
			vs->SetMatrix4x4("shadowProj1", shadowMap1.GetProjectionMatrix());
			vs->SetMatrix4x4("shadowView2", shadowMap2.GetViewMatrix());
			vs->SetMatrix4x4("shadowProj2", shadowMap2.GetProjectionMatrix());
			vs->SetShader();

			ps->SetData("light", &light, sizeof(DirectionalLight));
			ps->SetData("light2", &light2, sizeof(DirectionalLight));
			ps->SetData("camPos", &camPos, sizeof(DirectX::XMFLOAT3));

			std::vector<std::string>& texNames = renderables[i]->GetMaterial()->GetTexNames();
			std::vector<std::shared_ptr<SimpleTexture>>& textures = renderables[i]->GetMaterial()->GetTextures();
			for (size_t i = 0; i < textures.size(); ++i)
			{
				ps->SetShaderResourceView(texNames[i], (*(textures[i])).GetSRV());
			}

			ps->SetSamplerState("ss", renderables[i]->GetMaterial()->GetSampler());

			ps->SetSamplerState("ShadowSampler", shadowMap1.GetSampler());
			ps->SetShaderResourceView("ShadowMap1", shadowMap1.GetSRV());
			ps->SetShaderResourceView("ShadowMap2", shadowMap2.GetSRV());
			ps->SetShader();
			ps->CopyAllBufferData();
		}

		if (renderables[i]->GetChildCount() == 1) {
			ScriptEntity* scriptEntity = dynamic_cast<ScriptEntity*>(renderables[i]->GetChildren()[0]);
			if (scriptEntity) {

				MeltScript* melt = dynamic_cast<MeltScript*>(scriptEntity->GetScript());
				if (melt){ //&& melt->isActive()) {
					float t = melt->getTime();
					//if (t > 5)
					//	continue;
					ps->SetFloat("time", t);
					ps->CopyAllBufferData();
					vs->SetFloat("time", t);
				}
			}

		}
		if (i == ec - 1)//skybox better come in last
		{
			// Set the proper render states
			context->RSSetState(skybox.getRastState());
			context->OMSetDepthStencilState(skybox.getStencilState(), 0);
		}
		vs->SetMatrix4x4("world", renderables[i]->GetWorldMatrix());
		vs->CopyAllBufferData();

		context->IASetVertexBuffers(0, 1, renderables[i]->GetMesh()->GetVertexBuffer(), &stride, &offset);
		context->IASetIndexBuffer(*(renderables[i]->GetMesh()->GetIndexBuffer()), DXGI_FORMAT_R32_UINT, 0);
		context->DrawIndexed(renderables[i]->GetMesh()->GetIndexCount(), 0, 0);
	}

	// Reset the states!
	context->RSSetState(0);
	context->OMSetDepthStencilState(0, 0);

	// Time for particles
	particleVS->SetMatrix4x4("view", primaryCamera->GetViewMatrix());
	particleVS->SetMatrix4x4("projection", primaryCamera->GetProjMatrix());
	particleVS->SetShader();
	particleVS->CopyAllBufferData();
	particlePS->SetShader();

	// Particle states
	float blend[4] = { 1,1,1,1 };
	context->OMSetBlendState(particleBS, blend, 0xffffffff);
	context->OMSetDepthStencilState(particleDS, 0);

	// Update and draw emitters
	size_t pc = emitters.size();
	for (size_t i = 0; i < pc; ++i)
	{
		ParticleEntity* ent = emitters[i];
		ent->UpdateEmitters(deltaTime);

		for (int j = 0; j < ent->emitters.size(); ++j)
		{
			Emitter* emi = ent->emitters[j];

			for (int index = 0; index < emi->count; ++index)
			{
				int i = index * 4;

				DirectX::XMVECTOR pos = emi->particles[index].pos;

				DirectX::XMStoreFloat3(&(emi->particleVerts[i + 0].Position), pos);
				DirectX::XMStoreFloat3(&(emi->particleVerts[i + 1].Position), pos);
				DirectX::XMStoreFloat3(&(emi->particleVerts[i + 2].Position), pos);
				DirectX::XMStoreFloat3(&(emi->particleVerts[i + 3].Position), pos);

				emi->particleVerts[i + 0].Size = emi->particles[index].sizCur;
				emi->particleVerts[i + 1].Size = emi->particles[index].sizCur;
				emi->particleVerts[i + 2].Size = emi->particles[index].sizCur;
				emi->particleVerts[i + 3].Size = emi->particles[index].sizCur;

				float lifVal = emi->particles[index].time / emi->particles[index].life;
				DirectX::XMVECTOR pcb = emi->particles[index].colBeg;
				DirectX::XMVECTOR pce = emi->particles[index].colEnd;
				DirectX::XMVECTOR lif = DirectX::XMVectorSet(lifVal, lifVal, lifVal, lifVal);
				DirectX::XMVECTOR ili = DirectX::XMVectorSubtract(DirectX::XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f), lif);
				DirectX::XMVECTOR col = DirectX::XMVectorAdd(DirectX::XMVectorMultiply(lif, pcb), DirectX::XMVectorMultiply(ili, pce));

				DirectX::XMStoreFloat4(&(emi->particleVerts[i + 0].Color), col);
				DirectX::XMStoreFloat4(&(emi->particleVerts[i + 1].Color), col);
				DirectX::XMStoreFloat4(&(emi->particleVerts[i + 2].Color), col);
				DirectX::XMStoreFloat4(&(emi->particleVerts[i + 3].Color), col);
			}

			D3D11_MAPPED_SUBRESOURCE mapped = {};
			context->Map(emi->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped);
			memcpy(mapped.pData, emi->particleVerts, sizeof(ParticleVertex) * 4 * emi->count);
			context->Unmap(emi->vertexBuffer, 0);

			particlePS->SetShaderResourceView("tx", emi->texture->GetSRV());
			particlePS->CopyAllBufferData();

			UINT pstride = sizeof(ParticleVertex);
			UINT poffset = 0;
			context->IASetVertexBuffers(0, 1, &(emi->vertexBuffer), &pstride, &poffset);
			context->IASetIndexBuffer(particleIB, DXGI_FORMAT_R32_UINT, 0);

			context->DrawIndexed(emi->count * 6, 0, 0);
		}
	}

	// Reset to default states for next frame
	context->OMSetBlendState(0, blend, 0xffffffff);
	context->OMSetDepthStencilState(0, 0);

	// Unbind the shadow map so we don't have resource conflicts
	// at the start of the next frame
	if(ps!=0)
	ps->SetShaderResourceView("ShadowMap", 0);

	// Reset the states!
	context->RSSetState(0);
	context->OMSetDepthStencilState(0, 0);

	swapChain->Present(1, 0);
}

void Renderer::RenderShadowMaps()
{
	//this only needs to be called once per size, so batch this by size or make everything the same size.
	shadowMap1.PrepareContext(context);

	// Set up the vertex shader for shadow rendering
	SimpleVertexShader* shadowVS = shadowMap1.GetShadowVS();

	shadowVS->SetShader();
	DirectX::XMFLOAT4X4 view = shadowMap1.GetViewMatrix();
	DirectX::XMFLOAT4X4 projection= shadowMap1.GetProjectionMatrix();

	shadowVS->SetMatrix4x4("view", view);
	shadowVS->SetMatrix4x4("projection", projection);

	/*
	DirectX::XMMATRIX shProj = DirectX::XMMatrixOrthographicLH(
		10.0f,		// Width
		10.0f,		// Height
		0.01f,		// Near clip
		1000.0f);	// Far clip
	DirectX::XMStoreFloat4x4(&projection, XMMatrixTranspose(shProj));*/

	// Turn off the pixel shader
	context->PSSetShader(0, 0, 0);

	// Render all of our entities to the shadow map
	UINT stride = sizeof(Vertex);
	UINT offset = 0;

	for (unsigned int i = 0; i < renderables.size() - 1; i++)
	{
		// Grab the data from the first entity's mesh
		MeshEntity* ge = renderables[i];
		ID3D11Buffer** vb = ge->GetMesh()->GetVertexBuffer();
		ID3D11Buffer** ib = ge->GetMesh()->GetIndexBuffer();

		// Set buffers in the input assembler
		context->IASetVertexBuffers(0, 1, vb, &stride, &offset);
		context->IASetIndexBuffer(*ib, DXGI_FORMAT_R32_UINT, 0);

		DirectX::XMFLOAT4X4 wm = ge->GetWorldMatrix();


		shadowVS->SetMatrix4x4("world", wm);
		shadowVS->CopyAllBufferData();

		// Finally do the actual drawing
		context->DrawIndexed(ge->GetMesh()->GetIndexCount(), 0, 0);
	}

	RestoreContext();

	//this only needs to be called once per size, so batch this by size or make everything the same size.
	shadowMap2.PrepareContext(context);

	// Set up the vertex shader for shadow rendering
	shadowVS = shadowMap2.GetShadowVS();

	shadowVS->SetShader();
	view = shadowMap2.GetViewMatrix();
	projection = shadowMap2.GetProjectionMatrix();

	shadowVS->SetMatrix4x4("view", view);
	shadowVS->SetMatrix4x4("projection", projection);

	/*
	DirectX::XMMATRIX shProj = DirectX::XMMatrixOrthographicLH(
	10.0f,		// Width
	10.0f,		// Height
	0.01f,		// Near clip
	1000.0f);	// Far clip
	DirectX::XMStoreFloat4x4(&projection, XMMatrixTranspose(shProj));*/

	// Turn off the pixel shader
	context->PSSetShader(0, 0, 0);

	// Render all of our entities to the shadow map
	stride = sizeof(Vertex);
	offset = 0;

	for (unsigned int i = 0; i < renderables.size() - 1; i++)
	{
		// Grab the data from the first entity's mesh
		MeshEntity* ge = renderables[i];
		ID3D11Buffer** vb = ge->GetMesh()->GetVertexBuffer();
		ID3D11Buffer** ib = ge->GetMesh()->GetIndexBuffer();

		// Set buffers in the input assembler
		context->IASetVertexBuffers(0, 1, vb, &stride, &offset);
		context->IASetIndexBuffer(*ib, DXGI_FORMAT_R32_UINT, 0);

		DirectX::XMFLOAT4X4 wm = ge->GetWorldMatrix();


		shadowVS->SetMatrix4x4("world", wm);
		shadowVS->CopyAllBufferData();

		// Finally do the actual drawing
		context->DrawIndexed(ge->GetMesh()->GetIndexCount(), 0, 0);
	}

	RestoreContext();
}

void Renderer::RestoreContext()
{
	D3D11_VIEWPORT vp = {};
	vp.TopLeftX = 0.0f;
	vp.TopLeftY = 0.0f;
	vp.Width = (float)width;
	vp.Height = (float)height;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;

	// Revert all the DX settings for "regular" drawing
	context->OMSetRenderTargets(1, &backBufferRTV, depthStencilView);
	context->RSSetViewports(1, &vp);
	context->RSSetState(0); // Restores default state
}

void Renderer::OnResize(int width, int height)
{
	this->width = width;
	this->height = height;
}